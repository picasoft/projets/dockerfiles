# Bot de notification des mises à jour de services

Ce bot permet d'indiquer à l'équipe technique l'existence d'une mise à jour sur un service. Il utilise les API des forges logicielles pour récupérer la liste des versions ou un flux RSS (Atom 2005 seulement pour le moment mais facilement extensible) quand la première possibilité n'est pas disponible.

Il utilise *supercronic* afin d'effectuer régulièrement la récupération des nouvelles versions, il est possible de régler l'intervalle de récupération des informations par les variables d'environnement dans le fichier `docker-compose.yml`.

Pour ajouter ou enlever des sources de la liste il suffit de changer le fichier `feeds.json`, attention il faut idéalement que les id soient tous différents et ne pas réutiliser un id qui a été utilisé auparavant même s'il n'est plus utilisé.

Le nom peut changer sans problème, il est utilisé pour le message envoyé.

`url` correspond à l'url de l'endpoint de l'API pour récupérer les releases :
* `https://<gitlab.example.com>/api/v4/projects/<id du projet ou chemin encodé dans le format URL>/releases` pour une instance `GitLab`
* `https://api.github.com/repos/<organisation>/<dépôt>/releases` pour `GitHub`
* `https://<gitea.example.com>/api/v1/repos/<organisation>/<dépôt>/releases` pour une instance `Gitea`
ou à l'adresse du flux RSS.

`type` prend les valeurs `github`, `gitlab`, `gitea` ou `rss`.

Avant de lancer le service il faut renseigner un token de bot Mattermost ainsi que l'id du channel mattermost, il faut ajouter le bot à l'équipe et au channel souhaité.
