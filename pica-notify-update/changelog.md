# Changelog of notify services updates bot

## 0.3

* Send message with mention to @tech instead of @here

## 0.2

* The bot can now send mail
* Now a single mattermost post, utf8 encoded

## 0.1

* First version of this bot
* config in environment variables
* sources in json file
* compatible with gitea, github, gitlab, atom 2005
* can post message on Mattermost
