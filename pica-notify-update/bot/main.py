#! /usr/bin/python

import os
import requests
import json
import sqlite3
import xml.etree.ElementTree as ET
import smtplib
from email.message import EmailMessage

class Entries():
    def __init__(self, file_path, db_path):
        self.all_entries = []
        self.entries = []
        self.file_path = file_path
        self.db_path = db_path
        self.conn = sqlite3.connect(db_path)

    def init_entries(self):
        # Open config file
        with open(self.file_path) as f:
            feeds = json.load(f)
        # Execute for all feeds
        for feed in feeds:
            try:
                # Request
                req = requests.get(feed['url'])
                if req.status_code != 200:
                    print(f"Failed to get {feed['name']} at {feed['url']}, server returned {req.status_code}")
        
                # Check type and use a unique format in entries
                if feed['type'] == 'github':
                    for entry in req.json():
                        self.all_entries.append({'feed_id': feed['id'], 'tag_id': entry['id'], 'name': feed['name'], 'version': entry['tag_name'], 'url': entry['html_url']})
                elif feed['type'] == 'gitlab':
                    for entry in req.json():
                        self.all_entries.append({'feed_id': feed['id'], 'tag_id': entry['commit']['id'], 'name': feed['name'], 'version': entry['tag_name'], 'url': entry['_links']['self']})
                elif feed['type'] == 'gitea':
                    for entry in req.json():
                        self.all_entries.append({'feed_id': feed['id'], 'tag_id': entry['id'], 'name': feed['name'], 'version': entry['tag_name'], 'url': entry['html_url']})
                elif feed['type'] == 'rss':
                    root = ET.fromstring(req.content)
                    for entry in root.findall('{http://www.w3.org/2005/Atom}entry'):
                        self.all_entries.append({'feed_id': feed['id'], 'tag_id': entry.findtext('{http://www.w3.org/2005/Atom}id'), 'name': feed['name'], 'version': entry.findtext('{http://www.w3.org/2005/Atom}title'), 'url': entry.find('{http://www.w3.org/2005/Atom}link').get('href')})
                else:
                    print(f"Unsupported source type {feed['type']}")
            except:
                print(f"Failed to get {feed['name']} at {feed['url']}")
                raise
        return self.all_entries

    def get_entries(self):
        self.init_entries()

        c = self.conn.cursor()

        c.execute('CREATE TABLE IF NOT EXISTS entries (feed_id integer, tag_id text, name text, version text, url text)')

        for entry in self.all_entries:
            c.execute('SELECT tag_id FROM entries WHERE feed_id=? AND tag_id=?', (entry['feed_id'], entry['tag_id'],))
            if not c.fetchone():
                c.execute('INSERT INTO entries VALUES (?, ?, ?, ?, ?)', (entry['feed_id'], entry['tag_id'], entry['name'], entry['version'], entry['url'],))
                self.entries.append(entry)
            
            self.conn.commit()

        return self.entries

def show_updates():
    entries = Entries(os.getenv("CONFIG_PATH"), os.getenv('DB_PATH'))
    text = ""

    for entry in entries.get_entries():
        text += "\n* Une nouvelle version de " + entry['name'] +" est disponible [" + entry['version'] + "](" + entry["url"] + ") !"

    subject = os.getenv("MAIL_SUBJECT")
    to_address = os.getenv("MAIL_TO")
    from_address = os.getenv("MAIL_FROM")

    if text != "":
        send_email(text, subject, to_address, from_address)
        send_message("@tech\n# " + subject + "\n" + text)

def send_message(text):
    mm_server_url = os.getenv('MM_SERVER_URL')
    mm_channel_id = os.getenv('MM_CHANNEL_ID')
    mm_token = os.getenv('MM_TOKEN')

    req_headers = {'Authorization': 'Bearer ' + mm_token}
    req_data = '{"channel_id": "' + mm_channel_id + '", "message": "' + text.replace("\n", "\\n") + '"}'

    req = requests.post(mm_server_url, headers=req_headers, data=req_data.encode("utf-8"))

    if req.status_code != 201:
        print("Failed to post a message (error code:" + str(req.status_code) + "): " + req.text)
        return False

    return True

def send_email(text, subject, to_address, from_address):
    host = os.getenv('MAIL_HOST')
    port = os.getenv('MAIL_PORT')
    user = os.getenv('MAIL_USER')
    password = os.getenv('MAIL_PASSWORD')

    msg = EmailMessage()
    msg.set_content(text)
    msg['Subject'] = subject
    msg['From'] = from_address
    msg['To'] = to_address

    try:
        with smtplib.SMTP(host, port=port) as smtp:
            try:
                smtp.starttls()
                smtp.ehlo()
                print("Connected to SMTP server")
                try:
                    smtp.login(user, password)
                    try:
                        smtp.send_message(msg)
                        print("Email successfully sent")
                    except:
                        print("Failed to send message")
                        return
                except:
                    print("Failed to login")
                    return
            except:
                print("Failed to use StartTLS")
                return
    except:
        print("Failed to connect to the SMTP server")
        return

if __name__ == "__main__":
    show_updates()
    print("Updates messages sent")
