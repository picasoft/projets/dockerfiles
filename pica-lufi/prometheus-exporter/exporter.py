#!/usr/bin/env python3

"""Prometheus exporter for Lufi."""

# Imports
import os
import sys
import time
from datetime import datetime
import signal
from prometheus_client import start_http_server, Gauge, Histogram, REGISTRY, PROCESS_COLLECTOR, PLATFORM_COLLECTOR
import psycopg2


def db_connect():
    """
    Connect to Lufi database.
    :returns: psycopg2 connector
    """
    # Get database credentials
    host = os.getenv("EXPORTER_DB_HOST", "lufidb")
    port = int(os.getenv("EXPORTER_DB_PORT", "5432"))
    dbname = os.getenv("EXPORTER_DB_NAME", "lufi")
    user = os.getenv("EXPORTER_DB_USER", "lufidb")
    password = os.getenv("EXPORTER_DB_PASSWORD")
    if password is None:
        print("EXPORTER_DB_PASSWORD must be set.")
        return None

    # Connect to an existing database
    conn = psycopg2.connect(host=host, port=port, dbname=dbname, user=user, password=password)

    return conn


def get_hosted_files_count(db_conn):
    """
    Get the number of currently hosted files
    :param db_conn: Lufi database connector
    :returns: Count of hosted files for each retention duration day
    {
        1: 4,
        7: 16,
        15: 79
    }
    """
    data = {}
    # Create database cursor
    db_cursor = db_conn.cursor()
    # Query for existing file count, group by retention
    db_cursor.execute(
        "SELECT delete_at_day,COUNT(*) FROM files WHERE complete = 't' AND deleted = 'f' GROUP BY delete_at_day;"
    )
    for res in db_cursor.fetchall():
        data[res[0]] = res[1]
    # Close cursor
    db_cursor.close()
    return data


def get_files_count(db_conn):
    """
    Get the number of files that have been uploaded to Lufi
    :param db_conn: Lufi database connector
    :returns: Count of complete and incomplete files
    {
        "complete": 159,
        "incomplete": 12
    }
    """
    data = {}
    # Create database cursor
    db_cursor = db_conn.cursor()
    # Query for complete files count
    db_cursor.execute("SELECT COUNT(*) FROM files WHERE complete = 't';")
    data['complete'] = db_cursor.fetchone()[0]
    # Query for incomplete files count
    db_cursor.execute("SELECT COUNT(*) FROM files WHERE complete = 'f';")
    data['incomplete'] = db_cursor.fetchone()[0]
    # Close cursor
    db_cursor.close()
    return data


def get_downloads_count(db_conn):
    """
    Get the number of downloads for all files
    :param db_conn: Lufi database connector
    :returns: Count of downloads
    """
    # Create database cursor
    db_cursor = db_conn.cursor()
    # Query for complete files count
    db_cursor.execute("SELECT SUM(counter) FROM files WHERE complete = 't';")
    downloads_count = db_cursor.fetchone()[0]
    # Close cursor
    db_cursor.close()
    return downloads_count


def get_new_files_sizes(db_conn, start):
    """
    Get sizes (in bytes) for all files uploaded after a specific date
    :param db_conn: Lufi database connector
    :param start: UNIX timestamp for the start date
    :returns: A dict
    {
        "sizes": [], # The list of sizes in bytes
        "last_timestamp": 1612802678 # timestamp of the most recent new files
    }
    """
    data = {
        "last_timestamp": start,
        "sizes": []
    }
    # Create database cursor
    db_cursor = db_conn.cursor()
    # Query for complete files count
    db_cursor.execute(
        "SELECT created_at,filesize FROM files WHERE complete = 't' AND created_at > " +
        str(start) + " ORDER BY created_at DESC;"
    )
    for res in db_cursor.fetchall():
        data["sizes"].append(res[1])
        data["last_timestamp"] = res[0]
    # Close cursor
    db_cursor.close()
    return data


def main():
    """Main function"""
    # Number of seconds between 2 metrics collection
    collect_interval = int(os.getenv('EXPORTER_COLLECT_INTERVAL', "60"))
    # Port for metrics server
    exporter_port = 8000
    # Get instance name for metrics tag
    instance_name = os.getenv('INSTANCE_NAME')
    if instance_name is None:
        print("INSTANCE_NAME must be set.")
        return None

    # Connect to Lufi database
    db_conn = db_connect()
    if db_conn is None:
        print("Cannot connect to Lufi database.")
        sys.exit(-1)

    # Define an exit function to close connection
    def exit_handler(sig, frame):
        print('Terminating...')
        # Close database connection
        db_conn.close()
        sys.exit(0)

    # Catch SIGINT and SIGTERM signals
    signal.signal(signal.SIGINT, exit_handler)
    signal.signal(signal.SIGTERM, exit_handler)

    # Remove unwanted Prometheus metrics
    [REGISTRY.unregister(c) for c in [
        PROCESS_COLLECTOR,
        PLATFORM_COLLECTOR,
        REGISTRY._names_to_collectors['python_gc_objects_collected_total']
    ]]

    # Start Prometheus exporter server
    start_http_server(exporter_port)

    # Register metrics
    hosted_files_gauge = Gauge(
        'lufi_hosted_files_total',
        'Number of currently hosted files',
        ['instance_name', 'days_retention']
    )
    files_gauge = Gauge(
        'lufi_files_count',
        'Count of files that have been uploaded to Lufi, including deleted ones',
        ['instance_name', 'state']
    )
    downloads_gauge = Gauge('lufi_downloads_count', 'Count of downloads on the Lufi server', ['instance_name'])
    files_size_histogram = Histogram(
        'lufi_files_size_bytes',
        'Histogram of uploaded file sizes in bytes',
        ['instance_name'],
        buckets=(1000, 10000, 100000, 1000000, 10000000, 100000000, 1000000000, 2000000000, float("inf"))
    )
    # Initialize a timestamp to get only add new files to histogram
    cursor_ts = int(datetime.now().timestamp())

    # Loop forever
    while True:
        # Update hosted files
        hosted_files = get_hosted_files_count(db_conn)
        for retention_duration in hosted_files:
            hosted_files_gauge.labels(
                instance_name=instance_name,
                days_retention=retention_duration
            ).set(hosted_files[retention_duration])

        # Update files count
        files_counts = get_files_count(db_conn)
        for file_state in files_counts:
            files_gauge.labels(
                instance_name=instance_name,
                state=file_state
            ).set(files_counts[file_state])

        # Update downloads
        downloads_count = get_downloads_count(db_conn)
        downloads_gauge.labels(instance_name=instance_name).set(downloads_count)

        # Update size histogram
        sizes_data = get_new_files_sizes(db_conn, cursor_ts)
        cursor_ts = sizes_data["last_timestamp"]
        for size in sizes_data["sizes"]:
            files_size_histogram.labels(instance_name=instance_name).observe(size)

        # Wait before next metrics collection
        time.sleep(collect_interval)


if __name__ == '__main__':
    main()
