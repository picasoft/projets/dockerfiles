## Lufi

Ce dossier comprend les fichiers nécessaires pour lancer une instance de [Lufi](https://framagit.org/fiat-tux/hat-softwares/lufi/), un service libre de partage de fichier chiffrés.

### Configuration

Le fichier de configuration principal est [lufi.conf](./lufi.conf).
Il peut recevoir des variables d'environnement avec la syntaxe suivante : `$ENV['<env_variable>']`.

Le thème est injecté directement dans l'image à partir du dossier [picadrop](./picadrop).
Il ne contient pas de modifications esthétiques pour le moment, juste une modification des URL (CGU...).

### Premier lancement

Pour le bon fonctionnement de l'exporter de métrique :

- s'assurer qu'un utilisateur en lecteur seule est disponible pour accéder à la base de donnée;
- ajouter une variable `METRICS_AUTH` dans le fichier `.env` avec la chaine d'identification htaccess pour accéder à l'endpoint Prometheus.

Copier les fichiers de secrets en enlevant le `.example` et remplacer les valeurs.

Vérifier que le dossier correspondant au `device` de `lufi-data` existe sur l'hôte et que le propriétaire est bien l'utilisateur correspondant à Lufi dans le LDAP.

Exemple pour un `device` qui vaut `/DATA/docker/lufi` et un utilisateur avec l'UID `5002` et le GID `502` (valeurs actuelles dans le LDAP) :

```bash
$ sudo chown -R 5002:502 /DATA/lufi
```

Ce dossier doit être stocké sur un disque dur et non sur un SSD, en bout de chaîne. Voir [cette documentation](https://wiki.picasoft.net/doku.php?id=technique:docker:good_practices:storage#cas_particulier).

### Lancement

```bash
docker-compose up -d
```

### Mise à jour

Jusqu'à ce que [cette issue](https://framagit.org/fiat-tux/hat-softwares/lufi/-/issues/209#note_793653) soit fermée, la branche `fix-209-cannot-send-mail` est utilisée.
Une fois qu'elle sera fermée, on pourra utiliser les numéros de release comme indiqué ci dessous. Il faudra changer l'URL de téléchargement dans le `Dockerfile` en décommentant le `wget` et en enlevant l'actuel.

Il suffit de changer `LUFI_BUILD_VERSION` dans le `Dockerfile` et de reconstruire l'image.
Aucune autre opération n'est nécessaire pour effectuer les migrations et relancer le service.

En revanche, on veillera à :

- Vérifier qu'aucun paramètre de configuration important n'est apparu dans le [template de configuration](https://framagit.org/fiat-tux/hat-softwares/lufi/-/blob/master/lufi.conf.template), sinon on le rajoutera.
- Vérifier qu'il n'y a pas eu d'amélioration majeure dans [le thème par défaut](https://framagit.org/fiat-tux/hat-softwares/lufi/-/tree/master/themes/default), sur lequel on se base, sinon on les rajoutera.

De manière générale, on regardera le [changelog](https://framagit.org/fiat-tux/hat-softwares/lufi/-/releases) pour ne rien rater!
