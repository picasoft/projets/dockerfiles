## Version 1.8.7

Aucun changement à part la mise à jour du code.

## Version 1.8.6-1

Cette version de l'image Docker n'introduit aucun changement dans la version de Etherpad, mais retire le module `ep_pads_stats` pour le remplacer par `ep_prometheus_exporter`

## Version 1.8.6

Ajout d'un script de suppression externe pour la suppression des pads, pour éviter une surcharge de l'instance lors de la boucle de suppression interne.

Séparation des fichiers Compose en deux dossiers différents pour plus de clarté et éviter de lancer les deux instances simultanément par erreur.

Passage de Node 10 à Node 14 pour profiter des améliorations, et en particulier des `worker_threads` utilisés par `threads.js`.

Passage du cache JavaScript à 24h, ajout du paramètre `cookie.sameSite`.

Suppression du plugin `ep_markdown` qui est plus cassé qu'autre chose...

Changement du mode d'affichage des commentaires pour mieux supporter plusieurs commentaires sur une seule ligne. Désactivation du surlignage qui crée une révision vide à chaque commentaire, et des révisions vides dans le timeslider. À tester niveau UX.

## Version 1.8.4

Double bump :
* Vers la 1.8.3 : https://github.com/ether/etherpad-lite/releases/tag/1.8.3
* Vers la 1.8.4 : https://github.com/ether/etherpad-lite/releases/tag/1.8.4

Concernant notre instance :

### Base de données :
* [Changement de la taille du cache d'index](https://gitlab.utc.fr/picasoft/projets/dockerfiles/-/commit/8f1056face52a5e37d71028acbc5750c777bde82), la performance devrait être **grandement** améliorée

### Application :
* Nouveau plugin pour retourner à la page d'accueil (bouton tout à gauche)
* Nouveau plugin d'administration, pour lister les pads, les supprimer, voir le nombre d'auteur, etc
* Nouveau plugin permettant d'envoyer une annonce à tous les utilisateurs connectés, utile pour les mises à jour
* On peut de nouveau créer des pads sur l'instance principale ; la rétention est de deux ans, évolution à surveiller.
* Gestion de l'instance week et principale avec la CI et avec la même image Docker (configuration différente, versionnée sur le dépôt)
* Suppression des plugins obsolètes/inutiles, en particulier les tables, le comptage des mots, "page view", taille de police
* La même page d'accueil est utilisée pour les deux instances.

### Technique :
* Passage des liens aux réseaux Docker pour une meilleure isolation
* Durée du HEALTHCHECK diminuée pour que Traefik prenne en compte Etherpad plus rapidement
* Suppression effective des pads ayant dépassé la durée de rétention **une fois par semaine**, pour éviter des problèmes de performances ; cf [Framasoft](https://framagit.org/framasoft/Etherpad/pad_delete_after_delay), qui sur le plugin concerné indique :
> But we have so many pads (more than 30k on some instances) that the plugin fails to work correctly: it causes a really huge load on the etherpad processus at start and stuck it.

* Du coup, ne pas activer la suppression des pads ayant dépassé la durée de rétention à chaque redémarrage, pour éviter que ça prenne 15 min à démarrer, lors d'un crash par exemple.
* Tous les paramètres sont pris en charge par des variables d'environnement : meilleur nommage et ajustements des fichiers settings.json
