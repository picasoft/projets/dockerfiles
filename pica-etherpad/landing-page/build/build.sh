#!/bin/sh

# Requirements:
#
# * 'optipng'
# * 'jpegoptim'
# * 'npm' (Node.js)
# * node package 'minify'

set -e

# Change directory to the parent folder of this script

source_dir="$( dirname $0 )"
cd "$source_dir"

MINIFY="/opt/etherpad-lite/node_modules/.bin/minify"

# Minify a file in the format name.extension to name.min.extension
# Usage: minify_file <path_to_file_without_extension> <extension>
minify_file() {
  if [ -f $1.$2 ]; then
    echo -n "Minifying '$1.$2'  ...  "
    cat $1.$2 | $MINIFY --$2 > $1.min.$2 && echo "OK."
  else
    echo "Error: $1.$2 not found"
  fi
}

echo_blue() {
  echo -e "\033[1;34m\n$1\n\033[0m"
}

# Main program

echo_blue "(1/3): Optimize PNG images"
optipng ../static/img/*.png

echo_blue "(2/3): Optimize JPG images"
jpegoptim ../static/img/*.jpg

echo_blue "(3/3): Minify JS, CSS and HTML files"
minify_file ../static/js/etherpad js
minify_file ../static/vendor/fontello/css/fontello css
# Special case for html
echo -n "Minifying ../index.full.html  ...  "
cat ../index.full.html | $MINIFY --html > ../index.html && echo "OK."

echo_blue "Build successfull"
