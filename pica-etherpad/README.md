# Picapad

Ce dossier contient une image d'Etherpad Lite maintenue par l'association.

Pour une documentation générale (administration, maintenance), voir [le wiki](https://wiki.picasoft.net/doku.php?id=technique:adminserv:etherpad:start).

Les dossiers `standard` et `week` contiennent un fichier Compose permettant de lancer une instance : la différence majeure est la durée de la politique de rétention.

## Lancement et tests

Les documentations génériques [pour les tests](https://wiki.picasoft.net/doku.php?id=technique:docker:picasoft:test) et [pour le lancement](https://wiki.picasoft.net/doku.php?id=technique:docker:picasoft:admin#lancement_d_un_nouveau_service) s'appliquent.

Il faudra simplement veiller à utiliser les sous-dossiers `standard` et `week` au lieu de ce sous-dossier ci.

## Mise à jour de l'image

Pour mettre à jour la version d'Etherpad, il faut simplement modifir la variable `ETHERPAD_VERSION_BUILD` du [Dockerfile](./Dockerfile) et le tag des fichiers Compose de `week` et `standard`.

On vérifiera qu'il n'y a pas de nouveaux paramètres importants dans le fichier `settings.json`, sur l'[historique GitHub](https://github.com/ether/etherpad-lite/commits/develop/settings.json.docker).

Notez vos changements dans le fichier [CHANGELOG.md](./CHANGELOG.md).

Avant de mettre à jour le service en production, pensez à [prévenir tous les utilisateurs connectés](https://wiki.picasoft.net/doku.php?id=technique:adminserv:etherpad:admin_gui#message_a_tout_e_s).

## Configuration

Etherpad se configure au lancement du conteneur avec des variables d'environnement. Elles sont présentes à deux endroits :

- Les fichiers dans les sous-dossiers `secrets` contiennent les variables nécessaires pour créer une base de données, un utilisateur et stocker les mots de passe.
- Le reste des variables d'environnement non-confidentielles est affecté directement dans les fichiers Compose, via la directive `environment`.

Toutes les paramètres sont configurables via l'environnement. On pourra regarder le fichier [settings.json](./standard/settings.json) pour une référence. Des explications sont [disponibles ici](https://github.com/ether/etherpad-lite/blob/develop/settings.json.docker). Ce fichier contient uniquement les valeurs par défaut. La configuration doit avoir lieu dans les fichiers Compose.

Pour le bon fonctionnement des métriques, il faut aussi créer un fichier `.env` (dans le même dossier que le Docker Compose) qui devra contenir une variable `METRICS_AUTH`. Cette vairbale correspond à la chaîne d'identification htpasswd utilisée pour authentifier sur l'endpoint des métriques, par exemple `METRICS_AUTH="etherpad:$apr1$bXnknJ0S$GsC.ozNJc/dAkh9uH7Qlg."`

## Ajout d'un plugin

Etherpad maintient une [liste officielle des plugins](https://static.etherpad.org/plugins.html).

Pour installer un plugin, on évitera de passer par l'interface administrateur et on préfèrera modifier le [Dockerfile](./Dockerfile) directement.

Il suffit pour ce faire d'ajouter le nom du package npm dans le `Dockerfile`, sur la ligne `ARG ETHERPAD_PLUGINS`, en respectant l'ordre alphabétique pour la facilité de lecture.

## Page d'accueil

La page d'accueil est présente dans le dossier [landing-page](./landing-page) et est construite (_i.e._ compilée, minifiée...) automatiquement lors du build de l'image.

La même page d'accueil est utilisée pour les deux instances. Pour la modifier effectivement, il faudra pousser les modifications et reconstruire l'image en lançant manuellement le build via la CI.
