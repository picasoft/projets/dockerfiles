#!/usr/bin/env bash
set -e

if [ -z "${ADMIN_PASSWORD}" ]; then
	echo >&2 'Error : missing required ${ADMIN_PASSWORD} environment variable, exiting.'
	exit 1
fi
if [ -z "${DB_NAME}" ]; then
	echo >&2 'Error : missing required ${DB_NAME} environment variable, exiting.'
	exit 1
fi
if [ -z "${DB_USER}" ]; then
	echo >&2 'Error : missing required ${DB_USER} environment variable, exiting.'
	exit 1
fi
if [ -z "${DB_PASSWORD}" ]; then
	echo >&2 'Error : missing required ${DB_PASSWORD} environment variable, exiting.'
	exit 1
fi
if [ -z "${DB_HOST}" ]; then
	echo >&2 'Error : missing required ${DB_HOST} environment variable, exiting.'
	exit 1
fi

while ! PGPASSWORD="${DB_PASSWORD}" psql -h"${DB_HOST}" -U"${DB_USER}" -d"${DB_NAME}" -c "SELECT 1" &>/dev/null; do
		echo "Database server not ready yet, re-trying in 5 seconds..."
    sleep 5
done

exec "$@"
