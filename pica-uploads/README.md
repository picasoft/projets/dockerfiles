## Serveur SFTP et serveur web associé

Ce dossier contient les fichiers nécessaires pour faire tourner le serveur SFTP de chez Picasoft ainsi qu'un serveur web permettant d'accéder aux fichiers.

Il se base sur des images pré-construites et ne nécessite pas de construction manuelle d'image.

### Principe

Le Compose fournit permet de lancer deux services :
* Un serveur SFTP
* Un serveur web

Ces deux services accèdent au même volume Docker. Le serveur web sert le contenu du volume, tandis que le serveur SFTP écrit dans le volume.

Ce mécanisme permet de rendre immédiatement disponibles tous les fichiers uploadées via SFTP via le serveur web.

### Configuration

Le fichier de secrets contient la configuration nécessaire pour créer les utilisateurs autorisés à téléverser des fichiers sur le serveur SFTP.

La syntaxe utilisée [est documentée ici](https://hub.docker.com/r/atmoz/sftp/).

Notez que l'UID indiqué doit être celui de l'utilisateur `www-data` du serveur web, puisqu'il doit pouvoir accéder à ces fichiers. Le GID utilisé est celui du groupe `docker` de l'hôte, mais ce n'est pas un pré-requis.

### Lancement

Il suffit de copier le fichier `sftp.secrets.example` dans `sftp.secrets` et de choisir un mot de passe. Plusieurs utilisateurs peuvent être créés. Ce mot de passe doit être renseigné dans le [pass](https://gitlab.utc.fr/picasoft/interne/pass).

### Mise à jour

Il suffit de mettre à jour les tags des images utilisées dans le Docker Compose, de pousser les modifications sur ce dépôt et de relancer les nouveaux conteneurs.

### Utilisation

Voir la documentation utilisateur ici : https://wiki.picasoft.net/doku.php?id=technique:adminsys:sftp
