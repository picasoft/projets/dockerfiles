cat << EOF > /config.toml
port = 1965
host = "::"
log = "info"

[[server]]
hostname = "$HOSTNAME"
dir = "/var/gemini"
key = "/key.rsa"
cert = "/cert.pem"
index = "index.gmi"
lang = "fr"

[[server]]
hostname = "$MASTOGEM_HOSTNAME"
key = "/key.rsa"
cert = "/cert-toot.pem"
proxy_all = "mastogem:1965"
dir = "/var/gemini"
EOF

/gemserv /config.toml
