Fonctionnalités
===

Introduction
===
<i class="fa fa-file-text"></i> **CodiMD** est un éditeur markdown en temps réel, collaboratif et multi-plateforme. Cela signifie que vous pouvez écrire des notes avec d'autres personnes sur votre **ordinateur**, **tablette** ou même votre **smartphone**.
Vous pouvez vous connecter avec votre compte **[Picateam](https://team.picasoft.net)** ou créer un compte sur la *[page d'accueil](/)* (mais vous pouvez aussi faire sans).

Ce service est propulsé par le logiciel *[CodiMD](https://github.com/hackmdio/codimd)*. Si vous avez des problèmes, n'hésitez pas à nous contacter par mail, sur **Picateam** ou les réseaux sociaux.

Environnement
===
## Modes
**Ordinateur, Tablette**

<i class="fa fa-edit fa-fw"></i> Éditer : Vous ne voyez que l'éditeur.
<i class="fa fa-eye fa-fw"></i> Voir : Vous ne voyez que le résultat.
<i class="fa fa-columns fa-fw"></i> Les deux : Vous voyez les deux dans un écran partagé.

**Smartphone**

<i class="fa fa-toggle-on fa-fw"></i> Voir : Vous ne voyez que le résultat.
<i class="fa fa-toggle-off fa-fw"></i> Éditer : Vous ne voyez que l'éditeur.

## Mode sombre
Si vous êtes fatigué par l'écran blanc et préferez un mode sombre, cliquez sur la petite lune <i class="fa fa-moon-o"></i> pour activer le mode sombre de CodiMD.

L'éditeur, qui est en mode sombre par défaut, peut aussi changer de thème en cliquant sur le petit pinceau <i class="fa fa-paint-brush fa-fw"></i>.

## Téléchargement d'image
Vous pouvez simplement télécharger une image sur CodiMD en cliquant sur la caméra <i class="fa fa-camera"></i>.
Autrement, vous pouvez **glisser-déposer** l'image dans l'éditeur, ou même coller l'image.
L'image sera stockée sur nos serveurs.

## Partage des notes
Si vous voulez partager une note **éditable**, vous pouvez simplement partager L'URL.
Si vous souhaitez partager une note en **lecture seule**, appuez sur le bouton publier <i class="fa fa-share-square-o"></i> et copiez l'URL.

## Sauvegarder une note
Les notes sont enregistrées sur nos serveurs, mais vous pouvez télécharger vos notes en tant que fichier `.md` <i class="fa fa-file-text"></i> sur votre appareil.

## Importer des notes
Vous pouvez aussi importer des notes depuis le **presse-papier**, CodiMD peut même importer du contenu **html** ce qui peut s'avérer utile :smiley:.

## Permissions
Il est possible de changer les permissions d'accès à une note par le petit boutton en haut à droite du rendu.
Il y a plusieurs options :

|                              |Propriétaire en lecture/écriture|Utilisateur connecté en lecture|Utilisateur connecté en ecriture|Anonyme en lecture|Anonyme en ecriture|
|:-----------------------------|:--------------:|:------------:|:-------------:|:--------:|:---------:|
|<span class="text-nowrap"><i class="fa fa-leaf fa-fw"></i> **Freely**</span>               |✔|✔|✔|✔|✔|
|<span class="text-nowrap"><i class="fa fa-pencil fa-fw"></i> **Editable**</span>           |✔|✔|✔|✔|✖|
|<span class="text-nowrap"><i class="fa fa-id-card fa-fw"></i> **Limited**</span>           |✔|✔|✔|✖|✖|
|<span class="text-nowrap"><i class="fa fa-lock fa-fw"></i> **Locked**</span>               |✔|✔|✖|✔|✖|
|<span class="text-nowrap"><i class="fa fa-umbrella fa-fw"></i> **Protected**</span>        |✔|✔|✖|✖|✖|
|<span class="text-nowrap"><i class="fa fa-hand-stop-o fa-fw"></i> **Private**</span>       |✔|✖|✖|✖|✖|


**Seul le propriétaire d'une note peut changer les permissions (il faut donc que ce soit un utilisateur enregistré qui crée la note).**

## Intégration d'une note
Une note peut être intégrée à une page comme cela :

```xml
<iframe width="100%" height="500" src="https://md.picasoft.net/features" frameborder="0"></iframe>
```

## [Mode présentation](./slide-example)
Vous pouvez utiliser une syntaxe spéciale pour organiser vos notes en diapositives.
Ensuite, vous pouvez utiliser le **[Mode présentation](./slide-example)** <i class="fa fa-tv"></i> pour les présenter.
Le lien ci-dessus vous mènera vers plus de détails.

Pour changer la note en présentation, il faut mettre le [type de document](./yaml-metadata#type) en `slide`.

Voir
===
## Table des matières
Si vous regardez le coin en bas à droite de l'écran de rendu, il y a un bouton *table des matières* <i class="fa fa-bars"></i>.
Cliquer sur celui-ci fait apparaître la *table des matières*, de plus la section dans laquelle vous vous trouvez apparaîtra en surbrillance.
La table des matières fonctionne jusqu'à des titres de **3^e^ niveau**.

## Permalink
Chaque titre sera automatiquement accompagné d'un *Permalink* à sa gauche, il s'agit d'un lien qui mène automatiquement à la section concernée.
Vous pouvez survoler le titre et cliquer sur <i class="fa fa-chain"></i> pour copier le lien.

Éditer
===
## Modes d'édition
Si vous regardez dans le bas de l'aire d'édition à doite, vous trouverez un bouton nommé `sublime`.
Quand vous cliquez dessus, vous pouvez sélectionner 3 modes d'édition :

- sublime (par défaut)
- emacs
- vim

## Raccourcis clavier
Les raccourcis clavier dépendent du mode d'édtion sélectionné. Par défaut ils sont comme dans le logiciel Sublime text.
> Pour plus d'informations, voir [ici](https://codemirror.net/demo/sublime.html).

Pour emacs:
> Pour plus d'informations, voir [ici](https://codemirror.net/demo/emacs.html).

Pour vim:
> Pour plus d'informations, voir [ici](https://codemirror.net/demo/vim.html).

## Auto-complétion
L'éditeur fournit les indices d'auto-complétion pour markdown.
- Émojis : écrire `:` pour montrer les indices.
- Blocs de code : écrire ` ``` ` et un caractère pour montrer les indices. <i hidden>```</i>
- Titres : écrire `#` pour montrer les indices.
- Références : écrire `[]` pour montrer les indices.
- Intégrations : écrire `{}` pour montrer les indices.
- Images : écrire `!` pour montrer les indices.

## Titre
Le titre de la note sera par défaut le premier **titre de niveau 1**.

## Tags
Vous pouvez utiliser les tags comme suit, ils seront affichés dans votre **historique**.
###### tags: `fonctionnalités` `cool` `chatons`

## [Métadonnées YAML](./yaml-metadata)
Vous pouvez fournir des informations avancées sur la note pour impacter le comportement du navigateur (suivez le lien précédent pour plus de détails) :
- robots: information pour les robots
- lang: langage pour le navigateur
- dir: sens du texte
- breaks: utilisation des sauts de ligne
- slideOptions: options du mode présentation

## ToC
Utilisez `[TOC]` pour intégrer une table des matières directement dans la note.

[TOC]

## Émoji
Vous pouvez intégrer des émojis comme cela :smile: :smiley: :cry: :wink:
> Voir la liste complète des émojis [ici](http://www.emoji-cheat-sheet.com/).

## Liste de tâches
- [ ] ToDos
  - [x] Acheter de la salade
  - [ ] Se brosser les dents
  - [x] Boire de l'eau

## Blocs de code
CodiMD supporte plusieurs langages de programmation, utilisez l'auto-complétion pour voir la liste complète.
```javascript=
var s = "Coloration syntaxique du JavaScript";
alert(s);
function $initHighlight(block, cls) {
  try {
    if (cls.search(/\bno\-highlight\b/) != -1)
      return process(block, true, 0x0F) +
             ' class=""';
  } catch (e) {
    /* Gère l'exception */
  }
  for (var i = 0 / 2; i < classes.length; i++) {
    if (checkCondition(classes[i]) === undefined)
      return /\d+[\s/]/g;
  }
}
```
> Si vous voulez les **numéros de ligne**, insérez `=` après avoir spécifié le langage.
> Vous pouvez aussi spécifier le numéro de la première ligne.
> En dessous par exemple, les numéros de ligne commencent par le numéro 101:
```javascript=101
var s = "Coloration syntaxique du JavaScript";
alert(s);
function $initHighlight(block, cls) {
  try {
    if (cls.search(/\bno\-highlight\b/) != -1)
      return process(block, true, 0x0F) +
             ' class=""';
  } catch (e) {
    /* Gère l'exception */
  }
  for (var i = 0 / 2; i < classes.length; i++) {
    if (checkCondition(classes[i]) === undefined)
      return /\d+[\s/]/g;
  }
}
```

> Vous pouvez aussi vouloir conserver la numérotation du bloc de code précédent en utilisant `=+`

```javascript=+
var s = "Coloration syntaxique du JavaScript";
alert(s);
```

> Parfois vous avez un texte très long sans saut de ligne. Vous pouvez utiliser `!` pour activer les retours à la ligne dans le code.

```!
Lorsque vous êtes menuisier et que vous fabriquez une belle commode, vous n'allez pas utiliser un morceau de contreplaqué à l'arrière.
```

### Tags de citation
> Utilisez la syntaxe ci-dessous pour spécifier les **noms, date et couleur** des citations.
> [name=ChengHan Wu] [time=Sun, Jun 28, 2015 9:59 PM] [color=#907bf7]
> > Il y a même le support des citations imbriquées !
> > [name=ChengHan Wu] [time=Sun, Jun 28, 2015 10:00 PM] [color=red]

### Rendu du CSV en tableau

Vous pouvez utiliser du CSV dans un bloc de code :

~~~md
```csvpreview {header="true"}
Prénom,Nom,Email,Téléphone
John,Doe,john@doe.com,0123456789
Jane,Doe,jane@doe.com,9876543210
James,Bond,james.bond@mi6.co.uk,0612345678
```
~~~

Qui est rendu en :

```csvpreview {header="true"}
Prénom,Nom,Email,Téléphone
John,Doe,john@doe.com,0123456789
Jane,Doe,jane@doe.com,9876543210
James,Bond,james.bond@mi6.co.uk,0612345678
```

[Papa Parse](https://www.papaparse.com/) est utilisé pour parser le CSV. Les options peuvent être indiquées entre accolades: `{}`, les options sont séparées par des espaces. e.g. `{header="true" delimiter="."}`. Vous pouvez lire [leur documentation](https://www.papaparse.com/docs#config) comme référence.

## Intégration

### YouTube
```
{%youtube aqz-KE-bpKQ %}
```

### Vimeo
```
{%vimeo 124148255 %}
```

### Gist
```
{%gist schacon/4277%}
```

### SlideShare
```
{%slideshare briansolis/26-disruptive-technology-trends-2016-2018-56796196 %}
```

### PDF
**Attention : le PDF peut être bloqué par le navigateur si vous n'utilisez pas une URL `https`.**
```
{%pdf https://papers.nips.cc/paper/5346-sequence-to-sequence-learning-with-neural-networks.pdf %}
```

## MathJax

Vous pouvez intégrer des expressions mathématiques en *LaTeX* en utilisant **MathJax**, comme sur [math.stackexchange.com](http://math.stackexchange.com/) :

La *fonction Gamma* vérifiant $\Gamma(n) = (n-1)!\quad\forall n\in\mathbb N$ est par l'intégrale d'Euler

$$
x = {-b \pm \sqrt{b^2-4ac} \over 2a}.
$$

$$
\Gamma(z) = \int_0^\infty t^{z-1}e^{-t}dt\,.
$$

> Plus d'information à propos des expressions mathématiques **LaTeX** [ici](http://meta.math.stackexchange.com/questions/5020/mathjax-basic-tutorial-and-quick-reference).

## Diagrammes UML

### Diagrammes de séquence

Vous pouvez afficher des diagrammes de séquence comme cela :

```sequence
Alice->Bob: Salut bob, comment vas-tu ?
Note right of Bob: Bob pense
Bob-->Alice: Je vais bien, merci !
Note left of Alice: Alice répond
Alice->Bob: Où étais-tu ?
```

### Organigramme

Un organigramme peut-être rendu comme cela :
```flow
st=>start: Début
e=>end: Fin
op=>operation: Mon opération
op2=>operation: lalala
cond=>condition: Oui ou non ?

st->op->op2->cond
cond(yes)->e
cond(no)->op2
```

### Graphviz
```graphviz
digraph hierarchy {

                nodesep=1.0 // increases the separation between nodes

                node [color=Red,fontname=Courier,shape=box] //All nodes will this shape and colour
                edge [color=Blue, style=dashed] //All the lines look like this

                PDG->{ChefAtelier1 ChefAtelier2 Secrétaire}
                ChefAtelier1->{Ouvrier1 Ouvrier2}
                ChefAtelier2->Ouvrier3
                {rank=same;Ouvrier1 Ouvrier2 Ouvrier3}  // Put them on the same level
}
```

### Mermaid
```mermaid
gantt
    title Un diagramme de Gantt

    section Section
    Une tâche           :a1, 2014-01-01, 30d
    Une autre tâche     :after a1  , 20d
    section Une autre
    Tâche autre      :2014-01-12  , 12d
    Encore une autre tâche      : 24d
```

### Abc
```abc
X:1
T:Speed the Plough
M:4/4
C:Trad.
K:G
|:GABc dedB|dedB dedB|c2ec B2dB|c2A2 A2BA|
GABc dedB|dedB dedB|c2ec B2dB|A2F2 G4:|
|:g2gf gdBd|g2f2 e2d2|c2ec B2dB|c2A2 A2df|
g2gf g2Bd|g2f2 e2d2|c2ec B2dB|A2F2 G4:|
```

### PlantUML
```plantuml
start
if (condition A) then (oui)
  :Texte 1;
elseif (condition B) then (oui)
  :Texte 2;
  stop
elseif (condition C) then (oui)
  :Texte 3;
elseif (condition D) then (oui)
  :Texte 4;
else (rien)
  :Texte sinon;
endif
stop
```

### Vega-Lite
```vega
{
  "$schema": "https://vega.github.io/schema/vega-lite/v4.json",
  "data": {"url": "https://vega.github.io/editor/data/barley.json"},
  "mark": "bar",
  "encoding": {
    "x": {"aggregate": "sum", "field": "yield", "type": "quantitative"},
    "y": {"field": "variety", "type": "nominal"},
    "color": {"field": "site", "type": "nominal"}
  }
}
```

### Fretboad

```fretboard {title="horizontal, 5 frets", type="h6 noNut"}
-oO-*-
--o-o-
-o-oo-
-o-oO-
-oo-o-
-*O-o-
```

### Mindmap

```markmap
# markmap-lib

## Liens

- <https://markmap.js.org/>
- [GitHub](https://github.com/gera2ld/markmap-lib)

## En lien

- [coc-markmap](https://github.com/gera2ld/coc-markmap)
- [gatsby-remark-markmap](https://github.com/gera2ld/gatsby-remark-markmap)

## Fonctionnalités

- liens
- **style** ~~du~~ *texte*
- texte
  multiligne
```

> Plus d'informations à propos de la syntaxe **sequence diagrams** [ici](http://bramp.github.io/js-sequence-diagrams/).
> Plus d'informations à propos de la syntaxe **flow charts** [ici](http://adrai.github.io/flowchart.js/).
> Plus d'informations à propos de la syntaxe **graphviz** [ici](http://www.tonyballantyne.com/graphs.html)
> Plus d'informations à propos de la syntaxe **mermaid** [ici](http://mermaid-js.github.io/mermaid)
> Plus d'informations à propos de la syntaxe **abc** [ici](http://abcnotation.com/learn)
> Plus d'informations à propos de la syntaxe **plantuml** [ici](http://plantuml.com/index)
> Plus d'informations à propos de la syntaxe **vega** [ici](https://vega.github.io/vega-lite/docs)
> Plus d'informations à propos de la syntaxe **fretboard** [ici](https://hackmd.io/c/codimd-documentation/%2F%40codimd%2Ffretboard-syntax)

Zones d'alerte
---
:::success
Oui :tada:
:::

:::info
Ceci est un message :mega:
:::

:::warning
Regardez ! :zap:
:::

:::danger
ATTENTION ! :fire:
:::

:::spoiler Cliquez pour voir
Vous m'avez trouvé :stuck_out_tongue_winking_eye:
:::

## Typographie

### Titres

```
# Titre 1
## Titre 2
### Titre 3
#### Titre 4
##### Titre 5
###### Titre 6
```

### Lignes horizontales

___

---

***


### Substitutions typographiques

Activez l'option typographer pour voir le résultat.

(c) (C) (r) (R) (tm) (TM) (p) (P) +-

test.. test... test..... test?..... test!....

!!!!!! ???? ,,

Remarquable -- magnifique

"Guillemets doubles"

'Guillements simples'

### Mise en évidence

**Ce texte est en gras**

__Ce texte est en gras__

*Ce texte est en italique*

_Ce texte est en italique_

~~Ce texte est barré~~

normal~indice~

Exposant : 19^th^

Indice : H~2~O

++Souligné++

==Texte marqué==

{texte à la base|texte au dessus}

### Citations


> Les citations peuvent être imbriquées
>> ...en utilisant plus de `>` les uns à côté des autres...
> > > ...ou avec des espaces entre eux.


### Listes

#### Non ordonnées

+ Créez une liste en commençant une ligne par `+`, `-` ou `*`
+ Faites une sous liste en indentant de 2 espaces :
  - Le changement du marqueur force le démarrage d'une nouvelle liste :
    * Ac tristique libero volutpat at
    + Facilisis in pretium nisl aliquet
    - Nulla volutpat aliquam velit
+ C'est facile !

#### Ordonnées

1. Lorem ipsum dolor sit amet
2. Consectetur adipiscing elit
3. Integer molestie lorem at massa


1. Vous pouvez utiliser une séquence de nombres...
1. ...ou garder tous les nombres à `1.`
1. feafw
2. 332
3. 242
4. 2552
1. e2

Commencez à numéroter avec un décalage :

57. foo
1. bar

### Code

Code dans une ligne `code`

Code indenté

    // Un commentaire
    ligne de code 1
    ligne de code 2
    ligne de code 3


Bloc de code

```
Un texte...
```

Coloration syntaxique

``` js
var foo = function (bar) {
  return bar++;
};

console.log(foo(5));
```

### Tableaux

| Option | Description |
| ------ | ----------- |
| data   | Chemin vers le fichier à parser. |
| engine | Moteur à utiliser pour parser, *Handlebars* est utilisé par défaut. |
| ext    | Extension à utiliser pour la destination. |

Alignement des colonnes à droite

| Option | Description |
| ------:| -----------:|
| data   | Chemin vers le fichier à parser. |
| engine | Moteur à utiliser pour parser, *Handlebars* est utilisé par défaut. |
| ext    | Extension à utiliser pour la destination. |

Alignement des colonnes à gauche

| Option | Description |
|:------ |:----------- |
| data   | Chemin vers le fichier à parser. |
| engine | Moteur à utiliser pour parser, *Handlebars* est utilisé par défaut. |
| ext    | Extension à utiliser pour la destination. |

Colonnes centrées

| Option | Description |
|:------:|:-----------:|
| data   | Chemin vers le fichier à parser. |
| engine | Moteur à utiliser pour parser, *Handlebars* est utilisé par défaut. |
| ext    | Extension à utiliser pour la destination. |


### Liens
[Texte du lien](http://picasoft.net)
[Lien avec titre !](http://radio.picasoft.net "Podcasts La voix est libre")
Formatage automatique du lien https://team.picasoft.net


### Images
![Logo Picasoft](https://picasoft.net/res/picasoft-logo-no-text.png)
![Logo CHATONS](https://chatons.org/logo_chatons_v2.png "Logos CHATONS")
Comme pour les liens, les images ont aussi une syntaxe ressemblant aux notes de bas de page
![Logo GNU][id]
Avec une référence plus tard dans le document définissant l'URL.

[id]: https://upload.wikimedia.org/wikipedia/commons/thumb/3/39/Official_gnu.svg/langfr-800px-Official_gnu.svg.png  "GNU"

![Minion](https://chatons.org/logo_chatons_v2.png =200x200)
Afficher l'image avec une taille donnée

### Notes de bas de page

Note de bas de page 1[^first].
Note de bas de page 2[^second].
Note de bas de page^[Texte de la note intégrée] définie dans la ligne.
Duplication de la référence à une note de bas de page[^second].

[^first]: Les notes de bas de page **peuvent être stylisées**
    et multilignes.
[^second]: Texte de la note.

### Définitions

Terme 1

:   Définition 1
avec une suite sans intérêt.

Terme 2 avec *style dans la ligne*

:   Définition 2

        { du code, qui fait partie de la définition }

    Troisième paragraphe de la définition 2.

_Style compacté :_

Terme 1
  ~ Définition 1

Terme 2
  ~ Définition 2a
  ~ Définition 2b

### Abréviations

Ceci est un exemple d'abréviation HTML.
Elle convertie "HTML", mais garde intact les entrées partielles comme "xxxHTMLyyy" et autre.

*[HTML]: Hyper Text Markup Language
