# CodiMD

`CodiMD` est un service de pad en markdown avec des fonctionnalités plus avancées qu'`Etherpad`.

Les fichiers proposés dans ce dossier permettent la création de comptes via l'email, la connexion OAuth2 sur l'instance Mattermost de Picasoft et l'édition anonyme de tous les pads.

Le reste de la documentation pour CodiMD se trouve [sur le wiki](https://wiki.picasoft.net/doku.php?id=technique:adminserv:codimd:start).

## Lancer une instance

Copier les fichiers `.secrets.example` en `.secrets` et remplacer les valeurs. Les valeurs pour l'OAuth2 Mattermost peuvent se retrouver [ici](https://team.picasoft.net/picasoft/integrations/oauth2-apps).

Pour le bon fonctionnement des métriques, il faut aussi créer un fichier `.env` (dans le même dossier que le Docker Compose) qui devra contenir une variable `METRICS_AUTH`. Cette vairbale correspond à la chaîne d'identification htpasswd utilisée pour authentifier sur l'endpoint des métriques, par exemple `METRICS_AUTH="codimd:$apr1$bXnknJ0S$GsC.ozNJc/dAkh9uH7Qlg."`

Depuis le sous-dossier `pica-codimd`, lancer `docker-compose up -d`. Ceci a pour effet de lancer un conteneur pour `CodiMD` et un autre de `PostgreSQL` pour stocker les données.

## Configuration

Elle s'effectue via l'environnement, dans le fichier Compose. Voir les valeurs disponibles ici : https://hackmd.io/s/codimd-configuration

## Mettre à jour

Pour mettre à jour l'image il suffit de modifier le fichier `Dockerfile` pour changer la valeur de l'argument `VERSION`. Il faut aussi penser à changer la valeur du tag dans le fichier `docker-compose.yml`.

## Dictionnaires

Les dictionnaires proviennent de [freeoffice](https://www.freeoffice.com/fr/telecharger/dictionnaires), ils sont sous licence *MPL*, le fichier de licence est dans le dépôt, il s'agit de la version française toutes variantes.
