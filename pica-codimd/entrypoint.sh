#! /bin/sh

cat << EOF > /home/codimd/app/crontab
$DELETE_AT /home/codimd/app/deleteOldPad.py
EOF
sed "154a  | <a href=\"https://picasoft.net/co/cgu.html\">CGU</a> " -i /home/codimd/app/public/views/index/body.ejs
sed "41a <span style=\"font-size:2em;font-weight:bold;\">Cette instance ne garde le contenu que deux ans après la dernière modification !</span>" -i /home/codimd/app/public/views/index/body.ejs
/home/codimd/app/docker-entrypoint.sh &
/home/codimd/app/supercronic /home/codimd/app/crontab
