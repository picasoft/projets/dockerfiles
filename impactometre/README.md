# Impactomètre

Ce dossier contient les fichiers nécessaires pour construire des images Docker de l'impactomètre et le lancer.

> Impactometre.fr permet de comparer différents scénarios de réunions selon des critères environnementaux. Son objectif est de vous aider à faire des choix : est-il préférable d’organiser une visio-conférence ou de faire déplacer tous les participants ? etc.

> Impactometre.fr prend en compte les impacts environnementaux du matériel utilisé (ordinateurs, vidéoprojecteur…), des transports réalisés par les participants à la réunion mais aussi ceux de la visioconférence (transfert de données, logiciel…). L’utilisateur renseigne donc différentes informations pour décrire les scénarios de réunions envisagés.

Picasoft héberge cet outil à titre gratuit mais n'en est pas l'auteur.

Le code de l'impactomètre est disponible sur [GitHub](https://github.com/impactometre). Ce dépôt n'a vocation à accueillir aucune personnalisation ou modification : tout doit être fait sur l'upstream.

## Architecture

Très simple : un conteneur pour le back (application Node), et un pour le front (nginx servant une application Vue.js transpilée).

## Configuration

Le port interne utilisé par Node pour servir d'application est défini dans le [fichier Compose](./docker-compose.yml).

## Construction des images

```
docker-compose build
```

## Lancement

```
docker-compose up -d
```

## Mise à jour

Changer l'argument `VERSION` des Dockerfiles pour correspondre à une nouvelle release sur la forge utilisée (Github pour le moment), et reconstruire les images.
