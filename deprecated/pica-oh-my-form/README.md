## PicaForm

**This projet is considered broken and should not be used**.

Initially, a TX tried to create a service based on TellForm. The later has been deprecated and superseded by OhMyForm.

However, OhMyForm still has a lot of problems which are not Picasoft-compatibles, examples are :
* Calls to externals third party domains, i.e. heap.io, Google Analytics, etc.
* Production mode not working (only development mode)
* Lots, lots of scripts loaded (SPA-style)
* Some features broken

This service may be usable once the refactor is done (expected for v1.0), but for now we refuse to use it. 

### Introduction

This is a custom Dockerfile and Docker Compose of [OhMyForms](https://github.com/ohmyform/ohmyform/), made for the Picasoft infrastructure (Traefik in mind).

We removed development options and added some customization.

Each time you push to this directory :
* The image will be built and pushed on the registry
* A static and dynamic security analysis will be done

### Usage

Either you use the `docker-compose.yml` file by itself, either you integrate it in a global file. For the later, you can safely remove the `networks` directives.

You'll need to set some secrets in a file named `omf.secrets`. Take [`omf.secrets.example`] as a base. Be sure that the `env_file` directive in `docker-compose.yml` has the path of your secrets file.

Just launch `docker-compose up -d omf` and all should be ok.
