## InfluxDB Rhizome

**[Instance éteinte depuis cette discussion](https://team.picasoft.net/rhizome/pl/yab5quxx8tndtkmwyqnscn6f1y), pourra être rouverte plus tard!**

===========================================================
Nous hébergeons une base InfluxDB pour le compte de Rhizome.

Ce dossier contient les ressources nécessaires pour lancer l'instance.

### Lancement

Copier `influxdb-rhizome.secrets.example` dans `influxdb-rhizome.secrets` et remplacer les valeurs, puis lancer :

```bash
docker-compose up -d && docker-compose logs -f
```

Communiquer les identifiants aux personnes en charge chez Rhizome.

### Mise à jour

Il suffit de changer le tag dans [Compose](./docker-compose.yml).

Voir sur [la documentation](https://hub.docker.com/_/influxdb) si des étapes sont nécessaires pour passer d'une version majeure à une autre : ça devrait se faire sans soucis.
