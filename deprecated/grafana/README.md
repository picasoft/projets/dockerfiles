# Grafana

Grafana is a metrics visualization software. Picasoft use it to show some stats from our services. This image is basically just a copy of the one from Docker Hub.

## Configuration
There are some evironment variable you can use to configure the instance :
- `GF_DEFAULT_INSTANCE_NAME` : Name of your Grafana instance
- `GF_SERVER_ROOT_URL` : Root URL of your Grafana instance (eg. `https://grafana.picasoft.net`)
- `GF_SECURITY_ADMIN_USER` : Admin user
- `GF_SECURITY_ADMIN_PASSWORD` : Admin password

## Data persistence
In order to ensure data persistence for your Grafana datasources and dashboard, you should mount a volume on `/var/lib/grafana`. Because Grafana is not execute as root user, you need to specify correct access right to this volume. If using bind mount, you should do something like:

```bash
$ mkdir volumes/grafana && chown 472:472 volumes/grafana
```
