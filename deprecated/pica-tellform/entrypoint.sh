#!/bin/bash

set -e

: ${MONGODB_URI:=localhost}
: ${BASE_URL:=tellform.test.picasoft.net}
: ${MAILER_EMAIL_ID:=form}
: ${MAILER_PASSWORD:=loremipsumdolorsitamet}
: ${MAILER_FROM:=form@picasoft.net}
: ${MAILER_SMTP_HOST:=mail.picasoft.net}
: ${MAILER_SMTP_PORT:=587}
: ${MAILER_SMTP_SECURE:=TRUE}

#cat << EOF > /opt/tellform/.env
#MONGODB_URI=$MONGO_DB_URI
#BASE_URL=$BASE_URL
#MAILER_EMAIL_ID=$MAILER_EMAIL_ID
#MAILER_PASSWORD=$MAILER_PASSWORD
#MAILER_FROM=$MAILER_FROM
#MAILER_SMTP_HOST=$MAILER_SMTP_HOST
#MAILER_SMTP_PORT=$MAILER_SMTP_PORT
#MAILER_SMTP_SECURE=$MAILER_SMTP_SECURE
#EOF

cat << EOF > /opt/tellform/.env
NODE_ENV=development
CREATE_ADMIN=TRUE
PORT=3000
ADMIN_EMAIL=admin@admin.com
ADMIN_USERNAME=root
ADMIN_PASSWORD=password
MONGODB_URI=$MONGODB_URI
BASE_URL=$BASE_URL
MAILER_EMAIL_ID=$MAILER_EMAIL_ID
MAILER_PASSWORD=$MAILER_PASSWORD
MAILER_FROM=$MAILER_FROM
MAILER_SMTP_HOST=$MAILER_SMTP_HOST
MAILER_SMTP_PORT=$MAILER_SMTP_PORT
SIGNUP_DISABLED=FALSE
SUBDOMAINS_DISABLED=TRUE
ENABLE_CLUSTER_MODE=FALSE
APP_NAME=pica-form
APP_DESC=Une alternative OpenSource à TypeForm

EOF


#cat << EOF > /opt/tellform/.env                                                                                                                                          NODE_ENV=development                                                                                                                                                     CREATE_ADMIN_ACCOUNT=FALSE                                                                                                                                               PORT=3000                                                                                                                                                                ADMIN_EMAIL=admin@admin.com                                                                                                                                              ADMIN_USERNAME=root                                                                                                                                                      ADMIN_PASSWORD=password                                                                                                                                                  MONGODB_URI=$MONGODB_URI                                                                                                                                                 BASE_URL=$BASE_URL                                                                                                                                                       MAILER_EMAIL_ID=$MAILER_EMAIL_ID                                                                                                                                         MAILER_PASSWORD=$MAILER_PASSWORD                                                                                                                                         MAILER_FROM=$MAILER_FROM                                                                                                                                                 MAILER_SMTP_HOST=$MAILER_SMTP_HOST                                                                                                                                       MAILER_SMTP_PORT=$MAILER_SMTP_PORT                                                                                                                                       MAILER_SMTP_SECURE=TRUE                                                                                                                                                  SIGNUP_DISABLED=FALSE                                                                                                                                                    SUBDOMAINS_DISABLED=TRUE

exec "$@"
