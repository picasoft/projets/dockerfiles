# Localement : script de suppression des boards Wekan trop anciens
## Introduction

Ce README est obsolète mais peut-être testé en local, en dehors du serveur de picasoft, à condition de connaître les identifiants du compte Wekan dédié.

## Avant le lancement
Le paquet jq permet de parser et manipuler du JSON en ligne de commande. Il est nécessaire pour filtrer les informations reçu de l'API.

```bash
sudo apt install jq
```

Le Wekan doit contenir un compte dédié pour ce script permettant d'accéder à l'API. Ce compte doit disposer des droits administrateurs du Wekan. Les identifiants doivent être présents dans un fichier séparer du script `policy.sh`.

```bash
touch ids.sh
```

## Contenu de `ids.sh`
2 variables sont nécessaires : username et password du compte dédié.
```bash
#!/bin/sh

export username="username"
export password="password"
```

## Choix de la deadline et de l'URL de l'API
Les boards n'ayant pas été modifiés depuis un certain temps sont définitivement supprimés. Cette durée est la somme de trois variables : `boardExpirationDurationYears`, `boardExpirationDurationMonths` et `boardExpirationDurationDays`. L'URL de l'application est également nécessaire.
 Ces informations sont à modifier directement dans les premières lignes du script `policy.sh`.

```bash
boardExpirationDurationYears=0 # années
boardExpirationDurationMonths=12 # mois
boardExpirationDurationDays=0 # jours
url=https://wekan.test.picasoft.net
```


## Lancement du script
```bash
./policy.sh
```
