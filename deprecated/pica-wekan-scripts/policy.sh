#!/bin/sh


. ./ids.sh # contient $username et $password pour authentification

# Paramètres
boardExpirationDurationYears=${boardExpirationDurationYears-0} # années
boardExpirationDurationMonths=${boardExpirationDurationMonths-12} # mois
boardExpirationDurationDays=${boardExpirationDurationDays-0} # jours
url=${url-"https://wekan.test.picasoft.net"}
# Deadline = somme des durées du dessus

boardExpirationDurationDays=$(($boardExpirationDurationDays+30*$boardExpirationDurationMonths))
boardExpirationDurationDays=$(($boardExpirationDurationDays+365*$boardExpirationDurationYears))
deadLineSeconds=$(($boardExpirationDurationDays*86400))
now=$(date +%s)
deadlineTimeStamp=$(($now-$deadLineSeconds))

delete=false
while getopts "d" option
do
  delete=true
done
boardsToDelete=""
totalBoardsToDelete=0

# Récupération du token de connexion
queryPica=$(curl -s $url/users/login -d 'username='"$username"'&password='"$password"'')
token=$(echo $queryPica | jq -r .token)

# Récupération des utilisateurs
users=$(curl -s -X GET $url/api/users \
  -H 'Accept: application/json' \
  -H 'Authorization: Bearer '"$token"'')
usersIds=$(echo $users | jq -r '.[]._id')
usersUsernames=$(echo $users | jq -r '.[].username')

# Boucle sur les utilisateurs
for usersId in $usersIds
do
  # Récupération des boards de chaque utilisateur
  userBoards=$(curl -s -X GET $url/api/users/$usersId/boards \
  -H 'Accept: application/json' \
  -H 'Authorization: Bearer '"$token"'' | jq -r '.[]._id')

  # Boucle sur chaque board de l'utilisateur
  for userBoard in $userBoards
  do
    # Récupération de la date de dernière modification et du titre du board concerné
    board=$(curl -s -X GET $url/api/boards/$userBoard \
    -H 'Accept: application/json' \
    -H 'Authorization: Bearer '"$token"'')
    boardTitle=$(echo $board | jq -r '.title')
    lastModificationDate=$(echo $board | jq -r '.modifiedAt' | sed 's/T.*Z//g')
    # Si le board n'a jamais été modifié, on considère alors sa date de création
    if [ $lastModificationDate = null ]
    then
      lastModificationDate=$(echo $board | jq -r '.createdAt' | sed 's/T.*Z//g')
    fi

    if [ $(($deadlineTimeStamp-$(date -d $lastModificationDate +%s))) -gt 0 ]
    then
      if [ $delete = true ]
      then
        echo "Suppression du board "$boardTitle" (_id "$userBoard", modifié le "$lastModificationDate")"

        # Suppression du board si trop ancien
        curl -X DELETE https://wekan.test.picasoft.net/api/boards/$userBoard \
        -H 'Authorization: Bearer '"$token"''
        totalBoardsToDelete=$(($totalBoardsToDelete+1))
      elif [ $(echo $boardsToDelete | grep -c $userBoard) -eq 0 ]
      then
        echo "Board "$boardTitle" trop ancien (modifié le "$lastModificationDate")"
        boardsToDelete=$boardsToDelete" "$userBoard
        totalBoardsToDelete=$(($totalBoardsToDelete+1))
      fi
    fi
  done
done

if [ $delete = true ]
then
  echo "Total de boards supprimés : "$totalBoardsToDelete
else
  echo "Total de boards à supprimer : "$totalBoardsToDelete
fi


exit 1
