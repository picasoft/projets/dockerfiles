#!/bin/bash

# Check for mandatory environment variables
: "${SITE_NAME:?You have to set SITE_NAME environment variable}"
: "${ADMIN_MAIL:?You have to set ADMIN_MAIL environment variable}"
: "${ADMIN_PASSWORD:?You have to set ADMIN_PASSWORD environment variable}"

# Some default value
USER_ID=${USER_ID:-1234}
GROUP_ID=${GROUP_ID:-1234}

site=$(omd sites | grep $SITE_NAME)
if [ $? -ne 0 ]; then
  # Create the site
  omd create --uid $USER_ID --gid $GROUP_ID --admin-password $ADMIN_PASSWORD $SITE_NAME
  # Check that it worked
  if [ $? -ne 0 ]; then
    echo "Site creation failed ! Exiting..."
    exit 1
  fi
else
  # Check site versions
  version=$(echo $site | awk '{print $2}')
  if [ ! -z "${version##$CHECKMK_VERSION*}" ]; then
    echo "Invalid CheckMK version. This Docker image use $CHECKMK_VERSION and site $SITE_NAME is on $version. Exiting..."
    exit 1
  fi

  # Restore existing site by creating the user/group
  groupadd --force --gid $GROUP_ID $SITE_NAME
  useradd $SITE_NAME --uid $USER_ID --gid $GROUP_ID --home-dir /omd/sites/$SITE_NAME
  # Add this user to omd group
  usermod -a -G omd $SITE_NAME
  # Add www-data user to this new group
  usermod -a -G $SITE_NAME www-data
  # Set correct permissions on existing files
  chown -R $SITE_NAME:$SITE_NAME /omd/sites/$SITE_NAME

  # Override existing admin password
  htpasswd -b -m /omd/sites/$SITE_NAME/etc/htpasswd cmkadmin $ADMIN_PASSWORD

  # Enable the existing CheckMK site
  omd enable $SITE_NAME
fi

# Stop site before changing config
omd stop $SITE_NAME

# Disable ramdisk (needed inside Docker container)
omd config $SITE_NAME set TMPFS off

# Configure mail admin
omd config $SITE_NAME set ADMIN_MAIL $ADMIN_MAIL

# Configure web access
omd config $SITE_NAME set APACHE_TCP_ADDR 0.0.0.0
omd config $SITE_NAME set APACHE_TCP_PORT 5000

# Start site
omd start $SITE_NAME

# Function to terminate properly when using docker stop
_terminate() {
  echo "Caught SIGTERM signal!"
  omd stop $SITE_NAME
}
trap _terminate SIGTERM

# Just sleep forever since CheckMK only run in background
sleep infinity &
pid=$!
wait
