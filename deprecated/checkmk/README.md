# CheckMK

Docker image for [CheckMK](https://mathias-kettner.com/) monitoring system.

## Configuration
This image need some environment variables to be set in order to work:
- `SITE_NAME`: Name for the Checkmk site (eg `monitoring`)
- `ADMIN_MAIL`: Admin mail (which will be used to send alerts)
- `ADMIN_PASSWORD`: Password for admin account (login is `cmkadmin`)

You can also add some optionnal variables to customize your container:
- `USER_ID`: Force the site user UID (default to `1234`)
- `GROUP_ID`: Force the site user GID (default to `1234`)

## Deploy
Here is a `docker-compose.yml` example to deploy CheckMK behind Traefik.
```yaml
checkmk:
  image: registry.picasoft.net:5000/checkmk:1.4.0p19
  container_name: checkmk
  volumes:
    - /path/to/persistant/folder:/omd/sites
  environment:
    - SITE_NAME=monitoring
    - ADMIN_MAIL=contact@picasoft.net
    - ADMIN_PASSWORD=mypassword
  labels:
    - "traefik.frontend.rule=Host:checkmk.picasoft.net"
    - "traefik.port=5000"
  restart: always
```

## Upgrade
CheckMK provide a tool to upgrade a site version. Unfortunately this tools need that both CheckMK versions (old and new one) are installed. This is something tricky when working with containers, so the upgrade process of an existing site is quite manual. First, enter in your current container (with `docker exec -it checkmk bash`), install new CheckMK version and run upgrade process.
```bash
# Replace 1.4.0p22 with the new version you want to use
NEW_CHECKMK_VERSION=1.4.0p22
# Get new version
$ wget -O checkmk.deb "https://mathias-kettner.de/support/"$NEW_CHECKMK_VERSION"/check-mk-raw-"$NEW_CHECKMK_VERSION"_0.stretch_amd64.deb"
$ dpkg -i checkmk.deb
# Upgrade the site
$ omd stop $SITE_NAME
$ omd update $SITE_NAME
```
When everything is ok, you can exit and kill your container. Then build a new CheckMK image with the new version you want to use in your Dockerfile, and use this image to start again your CheckMK service. Your site will be working on the new CheckMK version.
