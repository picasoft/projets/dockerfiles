#!/usr/bin/env bash
docker run -v /root/mongo/data:/data/db -d --name mongoLatex -p 27017:27017 -p 28017:28017 -e MONGODB_USER="USERNAME" -e MONGODB_DATABASE="DBNAME" -e MONGODB_PASS="PASSWORD" dockerfile/mongodb
