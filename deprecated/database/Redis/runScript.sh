#!/usr/bin/env bash
docker pull redis
docker run -d -p 6379:6379 -v ~/PATH/TO/FOLDER:/data --name redis dockerfile/redis redis-server /etc/redis/redis.conf --requirepass PASSWORDHERE
