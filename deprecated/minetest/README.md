# Minetest server

Image to run a Minetest server. Listen on port 30000.

## Configuration
The Minetest server configuration is defined on a `minetest.conf` file.

### Mounted volumes
To have persistent data and configuration, you should mount some volumes on your host system :
- `/root/.minetest/minetest.conf` : Minetest configuration file
- `/root/.minetest/worlds` : Minetest maps
- `/root/.minetest/textures` : Minetest textures

### Log levels
To set the log level of your container, you can use the `LOGLEVEL` environment variable.
- 1 : Print to console errors only
- 2 : Print more information to console
- 3 : Print even more information to console
- 4 : Print enormous amounts of information to log and console

Default (and recommended) level is 1.

## Install Framinetest pack Framasoft
```bash
$ cd /tmp
$ wget https://framinetest.org/dl/worldmods.tar.gz https://framinetest.org/dl/textures.tar.gz

$ cd /PATH/TO/DATA/VOLUME/textures
$ tar xvf /tmp/textures.tar.gz

$ cd /PATH/TO/DATA/VOLUME/worlds
$ mkdir myworldname
$ cd myworldname
$ tar xvf /tmp/worldmods.tar.gz
```

## TODO
- Support other backend ? (For PostgreSQL see [this](https://wiki.minetest.net/Database_backends/fr) ?)
