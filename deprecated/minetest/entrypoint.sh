#!/usr/bin/env bash

# Check if there is a configurtion file
if [ ! -f /root/.minetest/minetest.conf ]
then
	# Use the default configuration file
	cp /etc/minetest/minetest.conf /root/.minetest/minetest.conf
fi

# Set loglevel
case "$LOGLEVEL" in
	1)
	  logoption="--quiet"
	  ;;
	2)
    logoption="--info"
		;;
	3)
    logoption="--verbose"
		;;
	4)
	  logoption="--trace"
    ;;
	*)
	  logoption="--quiet"
    ;;
esac

# Run the server
/usr/games/minetestserver "$logoption" --gameid minetest --config /root/.minetest/minetest.conf
