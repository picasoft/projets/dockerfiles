# -*- coding: utf-8 -*-

import re

from mattermost_bot.bot import listen_to
from mattermost_bot.bot import respond_to
import requests
import json

@listen_to('\\\link ^(http[s]?://[\S]*) (.*)*$')
@respond_to('\\\link ^(http[s]?://[\S]*) (.*)*$')
def link_display(message, url, display):
    payload = {
        'lsturl': url,
        'format': 'json'
    }
    r = requests.post('https://lstu.fr/a', data=payload)
    r = json.loads(r.text)
    message.comment('['+display+']'+'('+r['short']+')')

@listen_to('\\\link ^(http[s]?://[\S]*)$')
@respond_to('\\\link ^(http[s]?://[\S]*)$')
def link(message, url):
    payload = {
        'lsturl': url,
        'format': 'json'
    }
    r = requests.post('https://lstu.fr/a', data=payload)
    r = json.loads(r.text)
    message.comment(r['short'])


@listen_to('\\\link (.*\\n)*')
@respond_to('\\\link (.*\\n)*')
def link_multiple(message, opt):
    for elt in message.get_message().split('\n'):
        mes = elt.replace('\\link ','')
        if re.match('^(http[s]?://[\S]*)$', mes):
            link(message, mes)
        else:
            mes=mes.split(' ')
            link_display(message, mes[0], ' '.join(mes[1:]))
