# -*- coding: utf-8 -*-
import sys
reload(sys)
sys.setdefaultencoding("utf-8")

import re
from mattermost_bot.bot import listen_to
from mattermost_bot.bot import respond_to

@listen_to('hello|salut|bonjour|^yo$', re.IGNORECASE)
@respond_to('hello|salut|bonjour|^yo$', re.IGNORECASE)
def hello_comment(message):
    message.comment('Hey salut :wave:')


@respond_to('merci|thanks', re.IGNORECASE)
def thanks_comment(message):
    message.comment('No problemo :relaxed:')


@respond_to('help|aide|^man$')
@listen_to('help|aide|^man$')
def help(message):
    message.comment('Salut je suis le bot de Picasoft\nPour le moment, je sais réduire des liens avec la commande `\link <url> [<affichage>]`')


@listen_to('.* a \xe9t\xe9 ajout\xe9 au canal par.*',re.IGNORECASE)
@respond_to('.* a \xe9t\xe9 ajout\xe9 au canal par.*',re.IGNORECASE)
def add_channel(message):
    hello_comment(message)
