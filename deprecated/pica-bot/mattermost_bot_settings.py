import os

BOT_URL = os.environ['BOT_URL']
BOT_LOGIN = os.environ['BOT_LOGIN']
BOT_PASSWORD = os.environ['BOT_PASSWORD']
BOT_TEAM = os.environ['BOT_TEAM']
DEBUG = True
PLUGINS = [plugin for plugin in os.environ['PLUGINS'].split(',')]
