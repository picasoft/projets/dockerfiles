# postgres-backup

This image runs postgresdump to backup data using cronjob to folder `/backup`

## Usage:
```bash
docker run -d \
    --env POSTGRES_HOST=postgres.host \
    --env POSTGRES_PORT=27017 \
    --env POSTGRES_USER=admin \
    --env POSTGRES_PASS=password \
    --volume host.folder:/backup
    tutum/postgres-backup
```
## Parameters

Here are the parameters:
 - `POSTGRES_HOST` the host/ip of your postgres database
 - `POSTGRES_PORT` the port number of your postgres database
 - `POSTGRES_USER` the username of your postgres database
 - `POSTGRES_PASS` the password of your postgres database
 - `CRON_TIME`  the interval of cron job to run postgresdump. `0 0 * * *` by default, which is every day at 00:00
 - `POSTGRES_DB` the DB to backup
 - `MAX_BACKUPS` the number of backups to keep. When reaching the limit, the old backup will be discarded. No limit by default
 - `INIT_BACKUP` if set, create a backup when the container starts
 - `INIT_RESTORE_LATEST` if set, restores latest backup

## Restore from a backup

See the list of backups, you can run:

```bash
$ docker exec postgres-backup ls /backup
```

To restore database from a certain backup, simply run:
```bash
$ docker exec tutum-backup /restore.sh /backup/2015.08.06.171901
```
