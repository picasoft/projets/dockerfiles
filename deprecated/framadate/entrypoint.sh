#!/usr/bin/env bash

# Read environment variables or set default values
FRAMADATE_CONFIG=${FRAMADATE_CONFIG:-/var/www/framadate/app/inc/config.php}
DOMAIN=${DOMAIN-localhost}
APP_NAME=${APP_NAME-Framadate}
ADMIN_MAIL=${ADMIN_MAIL-}
NO_REPLY_MAIL=${NO_REPLY_MAIL-}
MYSQL_USER=${MYSQL_USER-user}
MYSQL_PASSWORD=${MYSQL_PASSWORD-password}
MYSQL_DB=${MYSQL_DB-framadate}
MYSQL_HOST=${MYSQL_HOST-mysql}
MYSQL_PORT=${MYSQL_PORT-3306}
DISABLE_SMTP=${DISABLE_SMTP-false}

# Add configuration file if not exist
if [ ! -f $FRAMADATE_CONFIG ]; then
  echo "There is no configuration file. Create one with environment variables"
  cp /var/www/framadate/app/inc/config.template.php $FRAMADATE_CONFIG
  # Set values on configuration file
  if [ ! -z "$DOMAIN" ]; then
    sed -i -E "s/^(\/\/ )?const APP_URL( )?=.*;/const APP_URL = '$DOMAIN';/g" $FRAMADATE_CONFIG
  fi
  sed -i -E "s/^(\/\/ )?const NOMAPPLICATION( )?=.*;/const NOMAPPLICATION = '$APP_NAME';/g" $FRAMADATE_CONFIG
  # Configure mail
  sed -i -E "s/^(\/\/ )?const ADRESSEMAILADMIN( )?=.*;/const ADRESSEMAILADMIN = '$ADMIN_MAIL';/g" $FRAMADATE_CONFIG
  sed -i -E "s/^(\/\/ )?const ADRESSEMAILREPONSEAUTO( )?=.*;/const ADRESSEMAILREPONSEAUTO = '$NO_REPLY_MAIL';/g" $FRAMADATE_CONFIG
  # Database configuration
  sed -i -E "s/^(\/\/ )?const DB_USER( )?=.*;/const DB_USER = '$MYSQL_USER';/g" $FRAMADATE_CONFIG
  sed -i -E "s/^(\/\/ )?const DB_PASSWORD( )?=.*;/const DB_PASSWORD = '$MYSQL_PASSWORD';/g" $FRAMADATE_CONFIG
  sed -i -E "s/^(\/\/ )?const DB_CONNECTION_STRING( )?=.*;/const DB_CONNECTION_STRING = 'mysql:host=$MYSQL_HOST;dbname=$MYSQL_DB;port=$MYSQL_PORT';/g" $FRAMADATE_CONFIG
  # SMTP config
  if [ "$DISABLE_SMTP" = "true" ]; then
    sed -i -E "s/'use_smtp' => true,/'use_smtp' => false,/g" $FRAMADATE_CONFIG
  fi
else
  echo "Using existing config file " $FRAMADATE_CONFIG
fi

# Configure /admin basic auth
if [ ! -f /var/www/framadate/admin/.htpasswd ]; then
  if [ "$ADMIN_USER" ] && [ "$ADMIN_PASSWORD" ]; then
    htpasswd -bc /var/www/framadate/admin/.htpasswd $ADMIN_USER $ADMIN_PASSWORD
  else
    echo "!!! You need to configure ADMIN_USER and ADMIN_PASSWORD environment variables !!!"
    exit 1
  fi
fi

# Run apache server
chown -R www-data:www-data /var/www/framadate
source /etc/apache2/envvars
exec apache2 -D FOREGROUND
