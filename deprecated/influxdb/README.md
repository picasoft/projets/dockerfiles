# InfluxDB

InfluxDB is a time series database. Picasoft use this FOSS TSDB to store some metrics about its services. This image is basically just a copy of the official one from Docker Hub.

## Configuration
To automatically create a database at startup, set this environment variable :
- `INFLUXDB_DB` : `DATABASENAME`

### Authentication
It is **really important** to properly configure authentication for InfluxDB HTTP API. First, to enable it, you should set this environment variable :
- `INFLUXDB_HTTP_AUTH_ENABLED` : `true`

Also, you need to use following environment variables in order to create some users :
- `INFLUXDB_ADMIN_USER` : An admin user, use for administration purpose only
- `INFLUXDB_ADMIN_PASSWORD` : Password for admin user
- `INFLUXDB_WRITE_USER` : A user that can be used to write data to your database
- `INFLUXDB_WRITE_USER_PASSWORD` : Password for the write user
- `INFLUXDB_READ_USER` : A user that can be used to read data from your database
- `INFLUXDB_READ_USER_PASSWORD` : Password for the read user

## Data persistence
In order to ensure data persistence for your database, you should mount a volume on `/var/lib/influxdb`
