# mattermost-cleaner

This image runs a `cron` to delete automatic messages from Mattermost teams (alpha version, only run on one team).
Delete command is run once when container starts and then every hour (depending on parameter) via `cron`.

**Warning** The commande delete directly data from database, which is bad pratice ; see [here](https://docs.mattermost.com/install/troubleshooting.html#important-notes) and [there](https://docs.mattermost.com/deployment/on-boarding.html#do-not-manipulate-the-mattermost-database)


## Usage:

```bash
$ docker run -d \
    --name mattermost-cleaner \
    --env POSTGRES_HOST=mattermost-db \
    --env POSTGRES_PORT=5432 \
    --env POSTGRES_USER=mattermost \
    --env POSTGRES_PASS=password \
    --env POSTGRES_DB=mattermost \
    --env MATTERMOST_TEAM=team \
    --net docker_default \
    --link mattermost-db:mattermost-db \
    registry.picasoft.net:5000/mattermost-cleaner:alpha
```
## Parameters

Here are the different parameters
 - `POSTGRES_HOST`      the name of the container
 - `POSTGRES_PORT`      the port number of the postgres database
 - `POSTGRES_USER`      the username of the postgres database
 - `POSTGRES_PASS`      the password of the postgres database
 - `POSTGRES_DB`        the name of the postgres database
 - `CRON_TIME`          the interval of cron job to run postgresdump. `0 * * * *` by default, which is every hour

## Docker-compose

```yaml
version: "2"
services:
  mattermost-cleaner:
    image: registry.picasoft.net:5000/mattermost-cleaner:alpha
    container_name: mattermost-cleaner
    links:
      - mattermost-db:mattermost-db
    environment:
      - POSTGRES_HOST=mattermost-db
      - POSTGRES_PORT=5432
      - POSTGRES_USER=mattermost
      - POSTGRES_PASS=password
      - POSTGRES_DB=mattermost
      - MATTERMOST_TEAM=team
```
