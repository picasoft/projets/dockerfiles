#!/usr/bin/env bash

[ -z "${POSTGRES_HOST}" ] && { echo "=> POSTGRES_HOST cannot be empty" && exit 1; }
[ -z "${POSTGRES_PORT}" ] && { echo "=> POSTGRES_PORT cannot be empty" && exit 1; }
[ -z "${POSTGRES_USER}" ] && { echo "=> POSTGRES_USER cannot be empty" && exit 1; }
[ -z "${POSTGRES_PASS}" ] && { echo "=> POSTGRES_PASS cannot be empty" && exit 1; }
[ -z "${POSTGRES_DB}" ] && { echo "=> POSTGRES_DB cannot be empty" && exit 1; }
[ -z "${MATTERMOST_TEAM}" ] && { echo "=> MATTERMOST_TEAM cannot be empty" && exit 1; }

export PGHOST=$POSTGRES_HOST
export PGPORT=$POSTGRES_PORT
export PGDATABASE=$POSTGRES_DB
export PGUSER=$POSTGRES_USER
export PGPASSWORD=$POSTGRES_PASS

SQL_CMD="DELETE FROM posts WHERE id IN (SELECT p.id FROM posts p LEFT JOIN channels c ON p.channelid=c.id LEFT JOIN teams t ON c.teamid=t.id WHERE (p.type='system_join_channel' OR p.type='system_join_team') AND t.name = '${MATTERMOST_TEAM}')"
psql -c "${SQL_CMD}"
