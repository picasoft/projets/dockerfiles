#!/bin/sh
echo pica-copying certs for ${DOMAIN} from /DATA/docker/traefik/certs/acme.json to /DATA/docker/mail/ssl
cat /DATA/docker/traefik/certs/acme.json | jq -r --arg domain ${DOMAIN} '.Certificates[] | if .Domain.Main == $domain then . else empty end | .Certificate' | base64 -d > /DATA/docker/mail/ssl/cert
cat /DATA/docker/traefik/certs/acme.json | jq -r --arg domain ${DOMAIN} '.Certificates[] | if .Domain.Main == $domain then . else empty end | .Key' | base64 -d > /DATA/docker/mail/ssl/key
