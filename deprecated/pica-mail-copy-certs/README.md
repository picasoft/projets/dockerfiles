**Doit être remplacé par `pica-tls-certs-monitor`.**

# Mise à jour des certificats du serveur mail

Le script `update-certs-pica-mail.sh` contient les instructions nécessaires pour extraire les certificats générés par Traefik pour le domaine `mail.picasoft.net` et créer les fichiers nécessaires (certificat, clé privée) pour le serveur mail.

Exécutées hors de l'image Docker, rajoutez `export DOMAIN=mail.picasoft.net` avant les instructions.

Le souci est que Traefik gère les requêtes HTTP(S), mais pas TLS en général. Pour les autres services TCP, Traefik peut générer les certificats mais ne les servira pas, il faut les extraire et copier manuellement.

Ce système ne sera plus utile dès lors que [ce système](https://gitlab.utc.fr/picasoft/projets/dockerfiles/-/tree/acme-copy-certs-dev/pica-tls-certs-monitor) sera implémenté.
