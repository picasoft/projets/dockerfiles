#!/bin/bash

set -e

usage() {
  echo -e "usage:\t$0 DIRECTORY, e.g. $0 pica-mattermost"
  echo -e "\tDIRECTORY : name of the directory containing docker-compose.yml\n"
  echo "This script simulates the first launch of a service : it will recreate all existing volumes for the service"
  echo "to be sure that it works independently of the former configuration, and then launch 'docker-compose up -d'."
  echo "This way, you can test your Dockerfile | docker-compose on the testing VM as if it was a brand new VM."
  echo -e "\nAlso, it will temporarily replace all occurences of 'picasoft.net' by 'test.picasoft.net' for convenience."
  echo -e "\nThis script will also build and pull all the required images based on the docker-compose.yml file."
  echo -e "\nUSE THIS SCRIPT ONLY ON THE TESTING VM."
  exit 1
}

create_dumb_secrets() {
  if [[ -d secrets ]]; then
    cd secrets
    echo -e "\n==== Create dumb secret files ===="
    for f in *.secrets.example; do
        echo -e "\tFile $1/secrets/$(basename -- "$f" .secrets.example).secrets created"
        cp -- "$f" "$(basename -- "$f" .secrets.example).secrets"
    done
    cd ..
  fi
}

REPO_PATH=$(pwd)

if [[ $(hostname) != *"test"* ]]; then
  echo "ERROR : DO NOT USE OUTSIDE OF A TEST MACHINE !"
  usage
fi

if [[ $# -ne 1 ]]; then
  echo "ERROR : wrong number of arguments"
  usage
fi

echo -e "Starting procedure for \033[31m$1\e[0m..."

# In case the Docker Compose changed since latest local commit,
# we need to set all down before pulling
if [[ -d "$1" ]]; then
  # Go to the folder of the service which will be tested
  cd "$1"

  create_dumb_secrets

  echo -e "\n==== Stop and remove existing containers and volumes ===="
  docker-compose down -v
else
  echo "Error : directory does not exist ($1)"
  exit 1
fi

echo -e "\n==== Remove and re-create named external volumes ===="
for v in $(docker-compose config --volumes); do
  res=$(grep $v -A 1 docker-compose.yml | grep 'external' || true)
  if [ ! -z "$res" ]; then
    # Don't fail if volume does not exists
    docker volume rm "$v" || true
    docker volume create "$v"
  fi
done

echo -e "\n==== Remove old images ===="
# For some reasons, sometime docker-compose does not pull the newer image. Force this!
docker-compose config | grep "image:" | cut -d ':' -f 2- | xargs docker image rm || true

echo -e "\n==== Pull new versions of external images ===="
docker-compose pull

echo -e "\n==== Build custom images ===="
docker-compose build

# Except for registry URL, useful to push to production registry after build
echo -e "\n==== Replace production URL with testing URL in all files ===="
for f in $(grep -l -r ".picasoft.net" .); do
  echo -e "\tFound in" ${f}
  # Revert old changes so the script is idempotent
  sed -i "s/.test.picasoft.net/.picasoft.net/g" ${f}
  # Apply again
  sed -i "s/.picasoft.net/.test.picasoft.net/g" ${f}
  # And revert for the registry which is always the production one
  sed -i "s/registry.test.picasoft.net/registry.picasoft.net/g" ${f}
done

echo -e "\n==== Lauch $1 ===="
docker-compose up -d

# Revert old changes so the script is idempotent
echo -e "\n==== Replace testing URL with production URL in all files ===="
for f in $(grep -l -r ".test.picasoft.net" .); do
  echo -e "\tFound in" ${f}
  sed -i "s/.test.picasoft.net/.picasoft.net/g" ${f}
done

echo -e "\n==== Print logs (use Ctrl+C to stop) ===="
docker-compose logs -f
