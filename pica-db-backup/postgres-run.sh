#!/bin/bash

BACKUP_FOLDER=${BACKUP_FOLDER:-"/backup/"}

[ -z "${POSTGRES_HOST}" ] && { echo "=> POSTGRES_HOST cannot be empty" && exit 1; }
[ -z "${POSTGRES_PORT}" ] && { echo "=> POSTGRES_PORT cannot be empty" && exit 1; }
[ -z "${POSTGRES_USER}" ] && { echo "=> POSTGRES_USER cannot be empty" && exit 1; }
[ -z "${POSTGRES_PASS}" ] && { echo "=> POSTGRES_PASS cannot be empty" && exit 1; }
[ -z "${POSTGRES_DB}" ] && { echo "=> POSTGRES_DB cannot be empty" && exit 1; }

ping -c 1 -W 1 "${POSTGRES_HOST}" || { echo -e "\n=========== ${POSTGRES_HOST} not available, skipping backup... ===========\n"; exit 1; }

BACKUP_CMD="pg_dump -w -c --format=c > $BACKUP_FOLDER"'${BACKUP_NAME}'
RESTORE_CMD="pg_restore -c \$1"

# Make sure backup folder exists
mkdir -p "${BACKUP_FOLDER}"
##########################
# CREATING BACKUP SCRIPT #
##########################
backup_script_name="${POSTGRES_SERVICE_NAME}-backup.sh"

echo "=> ${POSTGRES_SERVICE_NAME}: Creating backup script"
rm -f "/$backup_script_name"

cat <<EOF >> "/$backup_script_name"
#!/bin/bash
export PGHOST=$POSTGRES_HOST
export PGPORT=$POSTGRES_PORT
export PGDATABASE=$POSTGRES_DB
export PGUSER=$POSTGRES_USER
export PGPASSWORD=$POSTGRES_PASS

DATE=\$(date +\%Y.\%m.\%d.\%H\%M\%S)
BACKUP_NAME=\$DATE.dump

echo "=> ${POSTGRES_SERVICE_NAME}: Backup started: \${BACKUP_NAME}"
if ${BACKUP_CMD} ;then
    echo "${POSTGRES_SERVICE_NAME}:  Backup succeeded"
else
    echo "${POSTGRES_SERVICE_NAME}: Backup failed"
    rm -rf $BACKUP_FOLDER\${BACKUP_NAME}
fi
EOF
chmod +x /$backup_script_name

###########################
# CREATING RESTORE SCRIPT #
###########################
restore_script_name="${POSTGRES_SERVICE_NAME}-restore.sh"
backup_log="${POSTGRES_SERVICE_NAME}_postgres_backup.log"

echo "=> ${POSTGRES_SERVICE_NAME}: Creating restore script"
rm -f "/$restore_script_name"

cat <<EOF >> /$restore_script_name
#!/bin/bash
export PGHOST=$POSTGRES_HOST
export PGPORT=$POSTGRES_PORT
export PGDATABASE=$POSTGRES_DB
export PGUSER=$POSTGRES_USER
export PGPASSWORD=$POSTGRES_PASS

echo "=> ${POSTGRES_SERVICE_NAME}: Restore database from \$1"
if ${RESTORE_CMD} ;then
    echo "${POSTGRES_SERVICE_NAME}: Restore succeeded"
else
    echo "${POSTGRES_SERVICE_NAME}: Restore failed"
fi
EOF
chmod +x /$restore_script_name

touch /$backup_log
tail -F /$backup_log &

if [ -n "${INIT_BACKUP}" ]; then
    echo "=> ${POSTGRES_SERVICE_NAME}: Create a backup on the startup"
    /$backup_script_name
elif [ -n "${INIT_RESTORE_LATEST}" ]; then
    echo "=> ${POSTGRES_SERVICE_NAME}: Restore latest backup"
    until nc -z $POSTGRES_HOST $POSTGRES_PORT
    do
        echo "waiting database container..."
        sleep 1
    done
    ls -d -1 $BACKUP_FOLDER* | tail -1 | xargs /$restore_script_name
fi

echo "${CRON_TIME} /$backup_script_name >> /$backup_log 2>&1" >> /crontab.conf
crontab  /crontab.conf
echo "=> ${POSTGRES_SERVICE_NAME}: Running cron job"
