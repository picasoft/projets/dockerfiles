## Version 1.3

* Switch from Python image to Sid image (because Mongo does not exists in Bullseye and PG12 only in Bullseye and Sid...)
* Update for PG12+
* Default cron hour at 03:00AM
