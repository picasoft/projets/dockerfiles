#!/usr/bin/env python3
import os
import sys
import json

# Take a variable and set it as environment variable
def add_value_to_key(value, key):
    if "MONGO" in key:
        if flag_mongo:
            os.environ[key] = os.environ[key] + "," + value
        else:
            os.environ[key] = value
    if "MYSQL" in key:
        if flag_mysql:
            os.environ[key] = os.environ[key] + "," + value
        else:
            os.environ[key] = value
    if "POSTGRES" in key:
        if flag_postgres:
            os.environ[key] = os.environ[key] + "," + value
        else:
            os.environ[key] = value

CONFIG_FILE = '/config/backup_config.json'

# Check if config file exists
if not os.path.isfile(CONFIG_FILE):
    sys.stderr.write("Error : {0} config file not found".format(CONFIG_FILE))
    sys.exit()

# Variables initialization
open_config_file = open(CONFIG_FILE)
services_list = json.load(open_config_file)
flag_mongo = False
flag_mysql = False
flag_postgres = False

for service in services_list:
    if services_list[service]["Type"] == "mongo":

        # Mongo DB handling
        add_value_to_key(service, "MONGO_SERVICE_NAME_LIST")
        add_value_to_key(services_list[service]["Host"], "MONGO_HOST_LIST")
        add_value_to_key(services_list[service]["Port"], "MONGO_PORT_LIST")
        add_value_to_key(services_list[service]["Database"], "MONGO_DB_LIST")
        add_value_to_key(services_list[service]["Init-Backup"], "MONGO_INIT_BACKUP_LIST")
        add_value_to_key(services_list[service]["Cron"], "MONGO_CRON_TIME_LIST")
        add_value_to_key(services_list[service]["Folder"], "MONGO_BACKUP_FOLDER_LIST")

        flag_mongo = True

    if services_list[service]["Type"] == "mysql":

        # MySQL DB handling
        add_value_to_key(service, "MYSQL_SERVICE_NAME_LIST")
        add_value_to_key(services_list[service]["Host"], "MYSQL_HOST_LIST")
        add_value_to_key(services_list[service]["Port"], "MYSQL_PORT_LIST")
        user = services_list[service]["User"]
        if user in os.environ:
            user = os.environ[user]
        add_value_to_key(user, "MYSQL_USER_LIST")
        password = services_list[service]["Password"]
        if password in os.environ:
            password = os.environ[password]
        add_value_to_key(password, "MYSQL_PASS_LIST")
        add_value_to_key(services_list[service]["Database"], "MYSQL_DB_LIST")
        add_value_to_key(services_list[service]["Init-Backup"], "MYSQL_INIT_BACKUP_LIST")
        add_value_to_key(services_list[service]["Cron"], "MYSQL_CRON_TIME_LIST")
        add_value_to_key(services_list[service]["Folder"], "MYSQL_BACKUP_FOLDER_LIST")
        add_value_to_key(services_list[service]["Options"], "MYSQL_EXTRA_OPTS_LIST")

        flag_mysql = True

    if services_list[service]["Type"] == "postgres":

        # PostgreSQL handling
        add_value_to_key(service, "POSTGRES_SERVICE_NAME_LIST")
        add_value_to_key(services_list[service]["Host"], "POSTGRES_HOST_LIST")
        add_value_to_key(services_list[service]["Port"], "POSTGRES_PORT_LIST")
        user = services_list[service]["User"]
        if user in os.environ:
            user = os.environ[user]
        add_value_to_key(user, "POSTGRES_USER_LIST")
        password = services_list[service]["Password"]
        if password in os.environ:
            password = os.environ[password]
        add_value_to_key(password, "POSTGRES_PASS_LIST")
        add_value_to_key(services_list[service]["Database"], "POSTGRES_DB_LIST")
        add_value_to_key(services_list[service]["Init-Backup"], "POSTGRES_INIT_BACKUP_LIST")
        add_value_to_key(services_list[service]["Cron"], "POSTGRES_CRON_TIME_LIST")
        add_value_to_key(services_list[service]["Folder"], "POSTGRES_BACKUP_FOLDER_LIST")

        flag_postgres = True
# Closing config file
open_config_file.close()
# Excecution of run.sh after creating all environment variables from .json file
os.execv("/scripts/run.sh", ['run.sh'])
