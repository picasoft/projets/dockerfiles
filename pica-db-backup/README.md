# Backup des bases de données des services

Cet outil orchestre le backup des différents services de Picasoft.

Il est capable de gérer les bases de données `postgres`, `mysql` ou `mongo` pour le moment.

Voir le [wiki](https://wiki.picasoft.net/doku.php?id=technique:adminsys:backup:db:scripts).

## Mise à jour

Effectuer les changements désirés dans le Dockerfile, effectuer les changements désirés dans la configuration, mettre à jour le fichier `CHANGELOG.md` et changer la version du service dans le `docker-compose.yml`.

Il n'est pas nécessaire de modifier le numéro de version pour une simple mise à jour de configuration.

## Configuration

Le fichier dans [`config/backup_config.json`](./config/backup_config.json) recensent les informations sur les différentes bases de données qui doivent être backupées.

Ce fichier **est** utilisé **en production**.
Il ne doit pas être modifié sur les machines sans être synchronisé avec le dépôt Git.

Attention : si la base de donnée tourne dans un réseau Docker isolé, comme [pica-etherpad](../pica-etherpad/docker-compose.yml), il faudra ajouter `db-backup` à ce réseau dans le [docker-compose.yml](./docker-compose.yml).

### Structure

 - Nom du service : Contient une structure de données contenant les informations relatives au service telles qu'indiquées ci-dessous
 - `Host` : Indique l'hôte de la base de données
 - `Port` : Indique le port pour se connecter au service
 - `Database` : Indique le nom de la base de données
 - `User` : Nom d'utilisateur ou nom d'une variable d'environnement (optionnel)
 - `Password` : Mot de passe ou nom d'une variable d'environnement (optionnel)
 - `Folder` : Indique le nom du dossier de backup utilisé par le script de backup et de rotation
 - `Cron` : Indique la fréquence de temps à laquelle les backups sont effectués par le script de rotation au format cron
 - `Init-Backup` : Indique si un backup doit être effectué au démarrage du service, en plus de la programmation du cron

Pour MySQL, `Options` permet de passer des options à l'outil de backup.

### Gestion des secrets

Afin de pouvoir versionner le fichier de configuration sans exposer les identifiants aux bases de données, on utilise le système suivant :
* Dans le JSON, on utilise un nom de variable d'environnement à la place de l'identifant, **sans le $**, *e.g.* `ETHERPAD_DB_USER`.
* Dans le fichier `db.secrets`, on renseigne la valeur de cette variable d'environnement.

La substitution est effectué automatiquement par l'outil.

## Lancement

On se synchronise simplement avec le dépôt et on copie `secrets/db.secrets.example` dans `secrets/db.secrets` et on renseigne les secrets (voir [Gestion des secrets]((#gestion-des-secrets))).

On lance ensuite le Docker Compose :

```
$ docker-compose up -d
```

## Notes diverses

### Backup des BDD PostgreSQL

Initialement, `postgres-run.sh` réalisait un dump de la base de données en _plain text_ avec `pg_dump -w -c` qui était ensuite compressé par `tar`. Ceci a été changé par eb54ac21 et 6828cfa5 pour utiliser le [format "custom"](https://www.postgresql.org/docs/current/app-pgdump.html) de `pg_dump`.

Le temps pour réaliser un backup et sa taille restent quasiment les mêmes:

```
-rw-r--r-- 1 root root 133694555 avril  2 04:00 2020.04.02.020001.tar.gz
-rw-r--r-- 1 root root 133700591 avril  2 05:00 2020.04.02.030001.tar.gz
-rw-r--r-- 1 root root 133704227 avril  2 06:00 2020.04.02.040001.tar.gz
-rw-r--r-- 1 root root 133002366 avril  2 06:28 2020.04.02.042736.dump
-rw-r--r-- 1 root root 133005154 avril  2 07:00 2020.04.02.050001.dump
-rw-r--r-- 1 root root 133019511 avril  2 08:00 2020.04.02.060001.dump
```

(Les heures dans les noms des fichiers à droite sont incorrectes, il faut ajouter 2h pour avoir l'heure correcte, les backups sont quasi instantanés)

Cependant, utiliser le format "custom" permet de:

* Ne pas avoir des fichiers temporaires (les fichiers `*.sql` avant compression) qui prennent beaucoup de place pendant quelques instants. On a ainsi un élément de risque en moins (ces fichiers auraient pu être à l'origine d'un disque plein et donc casser tous les services d'une VM).
* Avoir un dump dans un format plus souple, qui permet de sélectionner manuellement et réorganiser les items archivés pendant la restauration
