#!/usr/bin/env bash

if [ "${MYSQL_ENV_MYSQL_PASS}" == "**Random**" ]; then
        unset MYSQL_ENV_MYSQL_PASS
fi

BACKUP_FOLDER=${BACKUP_FOLDER:-"/backup/"}

MYSQL_HOST=${MYSQL_PORT_3306_TCP_ADDR:-${MYSQL_HOST}}
MYSQL_HOST=${MYSQL_PORT_1_3306_TCP_ADDR:-${MYSQL_HOST}}
MYSQL_PORT=${MYSQL_PORT_3306_TCP_PORT:-${MYSQL_PORT}}
MYSQL_PORT=${MYSQL_PORT_1_3306_TCP_PORT:-${MYSQL_PORT}}
MYSQL_USER=${MYSQL_USER:-${MYSQL_ENV_MYSQL_USER}}
MYSQL_PASS=${MYSQL_PASS:-${MYSQL_ENV_MYSQL_PASS}}

[ -z "${MYSQL_HOST}" ] && { echo "=> MYSQL_HOST cannot be empty" && exit 1; }
[ -z "${MYSQL_PORT}" ] && { echo "=> MYSQL_PORT cannot be empty" && exit 1; }
[ -z "${MYSQL_USER}" ] && { echo "=> MYSQL_USER cannot be empty" && exit 1; }
[ -z "${MYSQL_PASS}" ] && { echo "=> MYSQL_PASS cannot be empty" && exit 1; }

ping -c 1 -W 1 "${MYSQL_HOST}" || { echo -e "\n=========== ${MYSQL_HOST} not available, skipping backup... ===========\n"; exit 1; }

BACKUP_CMD="mysqldump -h${MYSQL_HOST} -P${MYSQL_PORT} -u${MYSQL_USER} -p${MYSQL_PASS} ${EXTRA_OPTS} ${MYSQL_DB} > $BACKUP_FOLDER"'${BACKUP_NAME}'

# Make sure backup folder exists
mkdir -p "${BACKUP_FOLDER}"
##########################
# CREATING BACKUP SCRIPT #
##########################

backup_script_name="${MYSQL_SERVICE_NAME}-backup.sh"

echo "=> ${MYSQL_SERVICE_NAME}: Creating backup script"
rm -f "/$backup_script_name"

cat <<EOF >> "/$backup_script_name"
#!/bin/bash

DATE=\$(date +\%Y.\%m.\%d.\%H\%M\%S)
BACKUP_NAME=\$DATE.sql

echo "=> ${MYSQL_SERVICE_NAME}: Backup started: \${BACKUP_NAME}"
if ${BACKUP_CMD} ;then
    echo " => Compress files $BACKUP_FOLDER\$DATE.tar.gz"
    tar -czf $BACKUP_FOLDER\$DATE.tar.gz $BACKUP_FOLDER\${BACKUP_NAME} && \
    rm -rf $BACKUP_FOLDER\$BACKUP_NAME && \
    echo "${MYSQL_SERVICE_NAME}: Backup succeeded"
else
    echo "${MYSQL_SERVICE_NAME}: Backup failed"
    rm -rf ${BACKUP_FOLDER}\${BACKUP_NAME}
fi
EOF
chmod +x /$backup_script_name

###########################
# CREATING RESTORE SCRIPT #
###########################

restore_script_name="${MYSQL_SERVICE_NAME}-restore.sh"

echo "=> ${MYSQL_SERVICE_NAME}: Creating restore script"
rm -f "/$restore_script_name"

cat <<EOF >> /$restore_script_name
#!/bin/bash
echo "=> ${MYSQL_SERVICE_NAME}: Restore database from \$1"
tar -xzvf \$1
output="\$(echo \$1 | awk -F'.tar.gz' '{print \$1".sql"}')"
if mysql -h${MYSQL_HOST} -P${MYSQL_PORT} -u${MYSQL_USER} -p${MYSQL_PASS} < \$output ;then
    echo "${MYSQL_SERVICE_NAME}: Restore succeeded"
else
    echo "${MYSQL_SERVICE_NAME}: Restore failed"
fi
rm -Rf \$output
EOF
chmod +x /$restore_script_name

backup_log="${MYSQL_SERVICE_NAME}_mysql_backup.log"

touch /$backup_log
tail -F /$backup_log &

if [ -n "${INIT_BACKUP}" ]; then
    echo "=> ${MYSQL_SERVICE_NAME}: Create a backup on the startup"
    /${backup_script_name}
elif [ -n "${INIT_RESTORE_LATEST}" ]; then
    echo "=> ${MYSQL_SERVICE_NAME}: Restore latest backup"
    until nc -z $MYSQL_HOST $MYSQL_PORT
    do
        echo "waiting database container..."
        sleep 1
    done
    ls -d -1 $BACKUP_FOLDER* | tail -1 | xargs /$restore_script_name
fi

#####################
# SET THE CRON RULE #
#####################
echo "${CRON_TIME} /$backup_script_name >> /$backup_log 2>&1" >> /crontab.conf
crontab  /crontab.conf
echo "=> ${MYSQL_SERVICE_NAME}: Running cron job"
