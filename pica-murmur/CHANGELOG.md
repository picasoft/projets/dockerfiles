## pica-murmur:1.3.0-fix_signals

The main `entrypoint.sh` script can now forward SIGUSR1 to `murmurd` multiple times.
