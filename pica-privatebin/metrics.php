<?php
// General variables, the values are replaced by the entrypoint script
$auth_user = "PB_AUTH_USER";
$auth_passwd = "PB_AUTH_PASSWD";

$host = "PB_HOST";
$port = "PB_PORT";
$name = "PB_NAME";
$user = "PB_USER";
$password = "PB_PASSWD";

// We return a plain text file
header('Content-type: text/plain');

// This page require authentification
if(!isset($_SERVER['PHP_AUTH_USER'])) {
	header("WWW-Authenticate: Basic realm=\"Picasoft's PrivateBin metrics\"");
	header('HTTP/1.0 401 Unauthorized');
	die("Authentification required !");
	exit;
}

if($_SERVER["PHP_AUTH_USER"] != $auth_user OR $_SERVER["PHP_AUTH_PW"] != $auth_passwd)
{
	header('HTTP/1.0 401 Unauthorized');
	echo("Bad user or password");
	exit;
}

// We check that all variables used for database connection aren't empty
if(empty($host) OR empty($port) OR empty($name) OR empty($user) OR empty($password))
{
	echo("Please check your environment variables !");
	exit;
}

// We connect database
$conn = new PDO("pgsql:host=$host;port=$port;dbname=$name;user=$user;password=$password");

if(!$conn)
{
	echo("Failed to connect database !");
	exit;
}

// Get total number of pastes
$req = $conn->query("SELECT COUNT(*) FROM paste");

if(!$req)
{
	echo("SQL request failed");
	exit;
}

$res = $req->fetch();
$pastes = $res[0];

// Get number of pastes with opendiscussion
$req = $conn->query("SELECT COUNT(*) FROM paste WHERE opendiscussion = 1");

if(!$req)
{
	echo("SQL request failed");
	exit;
}

$res = $req->fetch();
$opendiscussion_pastes = $res[0];

// Get number of pastes with burnafterreading
$req = $conn->query("SELECT COUNT(*) FROM paste WHERE burnafterreading = 1");

if(!$req)
{
	echo("SQL request failed");
	exit;
}

$res = $req->fetch();
$burnafterreading_pastes = $res[0];

// Get number of 1year pastes (365*24*60*60=31536000 sec)
$req = $conn->query("SELECT COUNT(*) FROM paste WHERE expiredate-postdate = 31536000");

if(!$req)
{
	echo("SQL request failed");
	exit;
}

$res = $req->fetch();
$oneyear_pastes = $res[0];

// Get number of 1month pastes (30*24*60*60=2592000 sec)
$req = $conn->query("SELECT COUNT(*) FROM paste WHERE expiredate-postdate = 2592000");

if(!$req)
{
	echo("SQL request failed");
	exit;
}

$res = $req->fetch();
$onemonth_pastes = $res[0];

// Get number of 1week pastes (7*24*60*60=604800 sec)
$req = $conn->query("SELECT COUNT(*) FROM paste WHERE expiredate-postdate = 604800");

if(!$req)
{
	echo("SQL request failed");
	exit;
}

$res = $req->fetch();
$oneweek_pastes = $res[0];

// Get number of 1day pastes (24*60*60=86400 sec)
$req = $conn->query("SELECT COUNT(*) FROM paste WHERE expiredate-postdate = 86400");

if(!$req)
{
	echo("SQL request failed");
	exit;
}

$res = $req->fetch();
$oneday_pastes = $res[0];

// Get number of 1hour pastes (60*60=3600 sec)
$req = $conn->query("SELECT COUNT(*) FROM paste WHERE expiredate-postdate = 3600");

if(!$req)
{
	echo("SQL request failed");
	exit;
}

$res = $req->fetch();
$onehour_pastes = $res[0];

// Get number of 10minutes pastes (10*60=600 sec)
$req = $conn->query("SELECT COUNT(*) FROM paste WHERE expiredate-postdate = 600");

if(!$req)
{
	echo("SQL request failed");
	exit;
}

$res = $req->fetch();
$tenminutes_pastes = $res[0];

// Get number of 1hour pastes (5*60=300 sec)
$req = $conn->query("SELECT COUNT(*) FROM paste WHERE expiredate-postdate = 300");

if(!$req)
{
	echo("SQL request failed");
	exit;
}

$res = $req->fetch();
$fiveminutes_pastes = $res[0];

// Get number of comments
$req = $conn->query("SELECT COUNT(*) FROM comment");

if(!$req)
{
	echo("SQL request failed");
	exit;
}

$res = $req->fetch();
$comments = $res[0];
?>
# HELP privatebin_pastes_count Number of pastes
# TYPE privatebin_pastes_count gauge
privatebin_pastes_count <?php echo($pastes); ?>

# HELP privatebin_opendiscussion_pastes_count Number of pastes with opendiscussion
# TYPE privatebin_opendiscussion_pastes_count gauge
privatebin_opendiscussion_pastes_count <?php echo($opendiscussion_pastes); ?>

# HELP privatebin_burnafterreading_pastes_count Number of pastes with burnafterreading
# TYPE privatebin_burnafterreading_pastes_count gauge
privatebin_burnafterreading_pastes_count <?php echo($burnafterreading_pastes); ?>

# HELP privatebin_oneyear_pastes_count Number of 1 year pastes
# TYPE privatebin_oneyear_pastes_count gauge
privatebin_oneyear_pastes_count <?php echo($oneyear_pastes); ?>

# HELP privatebin_onemonth_pastes_count Number of 1 month pastes
# TYPE privatebin_onemonth_pastes_count gauge
privatebin_onemonth_pastes_count <?php echo($onemonth_pastes); ?>

# HELP privatebin_oneweek_pastes_count Number of 1 week pastes
# TYPE privatebin_oneweek_pastes_count gauge
privatebin_oneweek_pastes_count <?php echo($oneweek_pastes); ?>

# HELP privatebin_oneday_pastes_count Number of 1 day pastes
# TYPE privatebin_oneday_pastes_count gauge
privatebin_oneday_pastes_count <?php echo($oneday_pastes); ?>

# HELP privatebin_onehour_pastes_count Number of 1 hour pastes
# TYPE privatebin_onehour_pastes_count gauge
privatebin_onehour_pastes_count <?php echo($onehour_pastes); ?>

# HELP privatebin_tenminutes_pastes_count Number of 10 minutes pastes
# TYPE privatebin_tenminutes_pastes_count gauge
privatebin_tenminutes_pastes_count <?php echo($tenminutes_pastes); ?>

# HELP privatebin_fiveminutes_pastes_count Number of 5 minutes pastes
# TYPE privatebin_fiveminutes_pastes_count gauge
privatebin_fiveminutes_pastes_count <?php echo($fiveminutes_pastes); ?>

# HELP privatebin_comments_count Number of comments
# TYPE privatebin_comments_count gauge
privatebin_comments_count <?php echo($comments); ?>
