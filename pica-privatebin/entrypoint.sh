#! /bin/sh

sed -i "s/usr = \"\"/usr = \"$PB_USER\"/" /var/www/html/cfg/conf.php
sed -i "s/pwd = \"\"/pwd = \"$PB_PASSWD\"/" /var/www/html/cfg/conf.php
sed -i "s/dsn = \"\"/dsn = \"pgsql:host=$PB_HOST;port=$PB_PORT;dbname=$PB_NAME\"/" /var/www/html/cfg/conf.php
sed -i "s/.*<h4 class=\"col-md-5 col-xs-8\">.*<\/h4>.*/<div class=\"col-md-5 col-xs-8\"><h4><?php echo I18n::_(\$NAME); ?> <small>- <?php echo I18n::_('Because ignorance is bliss'); ?><\/small><\/h4><br><a href=\"https:\/\/picasoft.net\/co\/cgu.html\">CGU<\/a><\/div>/" /var/www/html/tpl/bootstrap.php
sed -i "s/PB_HOST/$PB_HOST/; s/PB_PORT/$PB_PORT/; s/PB_NAME/$PB_NAME/; s/PB_USER/$PB_USER/; s/PB_PASSWD/$PB_PASSWD/; s/PB_AUTH_USER/$PB_AUTH_USER/; s/PB_AUTH_PASSWD/$PB_AUTH_PASSWD/" /var/www/html/metrics.php

/start.sh
