# PrivateBin

`PrivateBin` est un service de pastebin chiffré. Il permet le partage temporaire de fichiers textes ainsi que leur clone, la discussion et leur protection par un mot de passe.

## Lancer une instance

Il suffit de cloner le dépôt dockerfiles et une fois dans le sous-dossier `pica-privatebin` de lancer `docker-compose up -d`. Cette commance lance une instance de `pica-nginx` préconfigurée pour fonctionner avec `PrivateBin` et une instance de `postresql` pour les données.

## Mettre à jour

Après avoir vérifié qu'il n'y avait aucune modification dans le fichier de configuration, il suffit de changer la version de l'argument `VERSION` dans le fichier `Dockerfile` ainsi que le tag dans le fichier `docker-compose.yml`.

Ensuite il faut lancer le script `docker_test.sh` à la racine de ce dépôt pour tester que tout se déroule correctement. Une fois que tout est bon il faut pousser l'image sur le registre et sur la machine de production il faut lancer `docker-compose up -d`.

## Supprimer un fichier

Il peut arriver qu'un utilisateur demande la suppression d'un fichier de l'instance. Pour y arriver il faut se connecter au conteneur de la BDD `docker-compose exec privatebin-db psql -U privatebin` et lancer `DELETE FROM "paste" WHERE dataid='<id du paste>'` (l'id du paste se trouve entre le `?` et le `#` dans l'URL).
