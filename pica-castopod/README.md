# Castopod

Castopod est un service de podcast en ligne disponible à l'adresse [https://podcast.picasoft.net](https://podcast.picasoft.net). Le service est découpé en 3 conteneurs, un service de base de données MySQL, un conteneur qui s'occupe des traitements PHP (`back`) et un conteneur qui sert le contenu statique (`front`).

## Mettre à jour

Il suffit de mettre à jour le numéro de version dans le fichier `docker-compose.yml` et dans les `Dockerfile` du `front` **et** du `back`. Puis on lance un `docker-compose build`.

**Attention** : Le logiciel est encore en *alpha* les migrations des bases de données ne sont pas encore faites automatiquement, il faut vérifier les notes de release et les faire le cas échéant.

## Installation

Remplir les fichiers situés dans `secrets` et lancer le tout. Se rendre vers `/cp-auth` pour finir l'installation et créer le compte administrateur. Ensuite pour se connecter il faut se rendre vers `/cp-auth/login`, l'interface d'administration est disponible sur `/cp-admin`.
