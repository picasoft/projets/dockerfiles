## GraphBot

### Introduction

Ce dossier contient les fichiers nécessaires pour lancer [Docker Graph Bot](https://gitlab.utc.fr/picasoft/projets/graph-bot) sur l'infrastructure de Picasoft. Voir le projet pour la documentation de référence.

Le Dockerfile se base sur l'image officielle et rajoute un entrypoint permettant d'injecter les identifiants du serveur FTP au moment du lancement du conteneur, à partir de l'environnement. Sans cela, les identifiants seraient dans le fichier de configuration, ce qui empêche de le versionner.

### Configuration

La configuration se fait :
* Dans le [docker-compose.yml](./docker-compose.yml) pour les variables d'environnement (niveau de log, dossier de configuration...)
* Dans [config.json](./config.json) pour le comportement de DGB

Pour le premier lancement, dans la mesure où DGB interroge plusieurs machines virtuelles, dont certaines à distance, il est nécessaire de créer un dossier `certs` contenant les clés privées et certificats pour communiquer avec les socket Docker distants. Ce dossier est ignoré via le [.gitignore](../.gitignore).

Voir :
* [La nomenclature des fichiers](https://gitlab.utc.fr/picasoft/projets/graph-bot#hosts)
* [Le renouvellement des certificats pour se connecter à distance au socket Docker](https://wiki.picasoft.net/doku.php?id=technique:docker:admin:socket-certs) si les certificats sont expirés ou qu'ils n'existent pas sur la machine de lancement.

La configuration actuelle prévoit que DGB soit lancé sur la machine `monitoring`.

### Secrets

Au premier lancement, copier `./secrets/sftp.secrets.example` dans `./secrets/sftp.secrets` et remplacer par les valeurs de production.

### Mise à jour

Il suffit de changer le tag dans le [Dockerfile](./Dockerfile), de construire l'image, la pousser sur le registre de Picasoft et mettre à jour le Docker Compose.

Vérifier le cas échéant les changements de configuration dans les [releases](https://gitlab.utc.fr/picasoft/projets/graph-bot/-/releases).
