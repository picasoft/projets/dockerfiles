if [ -z ${MY_DOMAIN+x} ]
then
  >&2 echo "Fatal to opendkim set-up : MY_DOMAIN environment variable is unset"
  exit 1
fi
if [ -z ${DKIM_SELECTOR+x} ]
then
  >&2 echo "Fatal to opendkim set-up : DKIM_SELECTOR environment variable is unset"
  exit 1
fi
echo "pica-generating opendkim tables (rsakeys and signingdomains tables) for domain ${MY_DOMAIN} and selector ${DKIM_SELECTOR}"
rm /etc/dkimkeys/rsakeys.table
echo "${DKIM_SELECTOR}._domainkey.${MY_DOMAIN} ${MY_DOMAIN}:${DKIM_SELECTOR}:/etc/dkimkeys/${DKIM_SELECTOR}.${MY_DOMAIN}.rsa" > /etc/dkimkeys/rsakeys.table
rm /etc/dkimkeys/signingdomains.table
echo "*@${MY_DOMAIN} ${DKIM_SELECTOR}._domainkey.${MY_DOMAIN}" > /etc/dkimkeys/signingdomains.table
