# v1.0.3

- Re-open port 25 for incoming mails
- Move away ENV from Dockerfile (better to have only one self-documented place, ie Compose)
- Remove `smtps` service from `master.cf` as it is obsolete and not mapped to any host port
- Remove `virtual_minimum_uid` already set to the default and does not seem useful without `virtual_uid_maps`
- Remove unused files (old private key, old rsyslog config)
- Try to clarify some comments (not sure they are tho)
- Fix syntax for milter filters (from Romain's hotfix on monitoring)
# v1.0.2

Support for LDAPS

# v1.0.1

Update path for certificates, use environment variables for flexibilibity

# v1.0

Version initiale
