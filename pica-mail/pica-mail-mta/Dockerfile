FROM debian:stretch
LABEL maintainer="picasoft@assos.utc.fr"

# Use default answer for installation as no one can answer
ENV DEBIAN_FRONTEND noninteractive

RUN apt-get update -y \
  && apt-get install -y \
      less pflogsumm postfix postfix-ldap libsasl2-modules sasl2-bin opendkim opendkim-tools rsyslog opendmarc ca-certificates procps \
  && rm -rf /var/lib/apt/lists/*

# Copy DKIM and DMARC conf
COPY spam/opendkim.conf /etc/
COPY spam/opendmarc.conf /etc/
COPY spam/opendkim /etc/default/opendkim
COPY spam/opendmarc /etc/default/opendmarc
RUN mkdir /etc/opendmarc
COPY --chown=opendmarc:opendmarc spam/ignore.hosts /etc/opendmarc/ignore.hosts

# Copy authentication daemon configuration
COPY saslauthd-postfix /etc/default/

# Copy scripts for building configuration at startup
COPY opendkim-tables.sh /
COPY config.sh /
COPY entrypoint.sh /

ENTRYPOINT ["/entrypoint.sh"]
