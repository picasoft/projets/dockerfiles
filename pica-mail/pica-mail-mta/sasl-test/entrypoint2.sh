#!/bin/bash

/entrypoint.sh true

postfix stop
#source de la liste des adresses mail valides
#voir le fichier ldap-virtual-mailbox-maps pour plus de commentaires
cat <<EOF >> /etc/postfix/ldap-virtual-mailbox-maps
server_host = ${LDAP_PROTOCOL}://${LDAP_SERVER_HOSTNAME}:${LDAP_PORT}
search_base = ${LDAP_SEARCH_BASE}
query_filter = ${LDAP_VIRTUAL_MAILBOX_FILTER}
bind = yes
bind_dn = ${LDAP_BIND_DN}
bind_pw = ${LDAP_BIND_PW}
result_attribute = uid
EOF

#ajout de la source
postconf -e "virtual_mailbox_maps = ldap:/etc/postfix/ldap-virtual-mailbox-maps"

#on modifie temporairement les restrictions pour permettre à tout le monde d'envoyer du mail par notre serveur tant que la destination est autorisée (par les autres règles) et que celui qui tente de se servir du serveur est sur un réseau autorisé (typiquement l'hôte et le subnet)
postconf -e "mynetworks = 127.0.0.0/8"
#voir en dessous pour la config sasl
postconf -e "smtpd_recipient_restrictions = permit_sasl_authenticated, reject_unauth_destination"

postconf -e "smtpd_client_restrictions = "

#désactivation des restrictions sur le helo
postconf -e "smtpd_helo_restrictions = "

#configuration du local delivery agent sous la forme du serveur lmtp de dovecot connecté par socket ip (inet)
postconf -e "virtual_transport = lmtp:inet:${LMTP_LAN_HOSTNAME}:${LMTP_PORT}"
#:private/dovecot-lmtp

#utiliser le démon saslauthd. Il est contacté par des appels de fonction à une lib et retourne la validité des login.
postconf -e "smtpd_sasl_path = smtpd"
cat <<EOF >> /etc/postfix/sasl/smtpd.conf
pwcheck_method: saslauthd
mech_list: PLAIN LOGIN
EOF

#fichiers de config et socket utilisés par le démon de saslauthd créé pour postfix (voir fichier copié dans le Dockerfile)
dpkg-statoverride --add root sasl 710 /var/spool/postfix/var/run/saslauthd
adduser postfix sasl
service saslauthd  restart

#on utilise les comptes unix de l'hôte
postconf -e 'smtpd_sasl_local_domain = $myhostname'
postconf -e 'smtpd_sasl_auth_enable = yes'
postconf -e 'smtpd_sasl_security_options = noanonymous'
#autorise l'auth depuis des clients connus comme sécurisés mais utilisant des syntaxes obsolètes/non standard (=outlook)
postconf -e 'broken_sasl_auth_clients = yes'

useradd toto
useradd bobo
echo "toto:toutou"|chpasswd
echo "bobo:boubou"|chpasswd

if [ "$1" == "imap" ]
then

echo "L\'authentification SASL se fera par IMAP."
cat <<EOF >> /etc/default/saslauthd-postfix
MECHANISMS="rimap"
MECH_OPTIONS="${IMAP_AUTH_LAN_HOSTNAME}/${IMAP_AUTH_PORT}"
EOF

elif [ "$1" == "ldap" ]
then
echo "L\'authentification SASL se fera par LDAP."

cat <<EOF >> /etc/default/saslauthd-postfix
MECHANISMS="ldap"
MECH_OPTIONS=""
EOF

cat <<EOF >> /etc/saslauthd.conf
ldap_servers: ${LDAP_PROTOCOL}://${LDAP_SERVER_HOSTNAME}:${LDAP_PORT}
ldap_bind_dn: ${LDAP_BIND_DN}
ldap_bind_pw: ${LDAP_BIND_PW}
ldap_search_base: ${LDAP_SEARCH_BASE}
ldap_filter: ${LDAP_SASL_FILTER}
EOF

else
echo "L\'authentification SASL se fera par PAM."

cat <<EOF >> /etc/default/saslauthd-postfix
MECHANISMS="pam"
MECH_OPTIONS=""
EOF
fi

service saslauthd restart

postfix start
postfix reload
postfix stop

service rsyslog start

service postfix stop
service postfix start

tail -F /var/log/mail.log
