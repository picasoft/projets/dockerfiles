Ce conteneur permet de tester différentes implémentations de SASL: PAM, remote IMAP et LDAP.

Il peut être compilé et lancé ainsi:
```
docker build -t pica-mail-mta:2 .

docker run -it --name pica-mail-mta-ldap --network local-mail-delivery  --mount source=mail-mta-log,target=/var/log  -v /DATA/docker/mail/opendkim/nov2018.private:/etc/dkimkeys/nov2018.picasoft.net.rsa:ro pica-mail-mta:2 [ldap|pam|imap]
```
Sans argument, le comportement par défaut est PAM.

Ensuite, pour tester, il va falloir se connecter en SMTP, le plus simple est d'utiliser telnet. Vous pouvez par exemple exposer sur l'hôte le port 25, ou encore insérer un bash dans le conteneur. (Nous laissons au lecteur le choix de la solution la plus élégante.)

Voici un exemple simple:

```
telnet localhost 25
Trying 127.0.0.1...
Connected to localhost.
Escape character is '^]'.
220 pica01-test.test.picasoft.net ESMTP Postfix (Debian/GNU)
helo pica01-test.test.picasoft.net
250 pica01-test.test.picasoft.net
AUTH PLAIN AG1haWwxAG1haWwxcHdk
235 2.7.0 Authentication successful
quit
221 2.0.0 Bye
Connection closed by foreign host.
```
Pour générer la chaîne permettant de s'authentifier, il s'agit d'un bête base64, qu'on peut générer par exemple en utilisant perl:
```
perl -MMIME::Base64 -e     'print encode_base64("\0utilisateur\0motdepasse");'
```
\0 représente un octet nul.
Comme son nom l'indique, l'AUTH PLAIN est en texte clair. Il est donc important que le canal qui l'entoure soit sécurisé: SMTP avec une couche de SSL/TLS, ou encore un docker network.

Dans l'environnement de test de picasoft, on peut utiliser les couples (login, mot de passe) suivants:
* PAM : (toto, toutou)
* IMAP : (mail1, mail1pwd)
* LDAP : (jacques, blabla)
Dans le cas de IMAP et LDAP, on ne peut garantir que ces utilisateurs existent encore au moment où vous lisez ces lignes: ils sont censés être stockés respectivement dans le conteneur de test pica-mail-mda-auth_plaintext et dans le serveur LDAP de test test.picasoft.net.

On peut s'authentifier au serveur en utilisant des couples (login, mot de passe) totalement indépendant des boîtes mails. Ces couples viennent de la source choisie (PAM, rIMAP, LDAP). Il n'y a pas de bind avec la source permettant de contrôler l'existence des adresses mails*: le SASL permet seulement de protéger le serveur, mais une fois qu'on est dedans, on peut faire ce qu'on veut. Cependant, la configuration de postfix permettra (à terme) de réagir différemment selon que l'utilisateur est authentifié ou non. (Par exemple, on pourra autoriser tout le monde à envoyer du courrier à des adresses @picasoft.net, et autoriser seulement les utilisateurs authentifiés à envoyer du courrier en tant qu'une adresse @picasoft.net).

\*En effet, le conteneur reconnaît deux adresses et boîtes mails crées arbitrairement:  mail1@picasoft.net et mail2@picasoft.net. On peut donc envoyer des mails en tant que / à ces adresses, et recevoir des mails à ces adresses sur le MDA s'il est allumé.
