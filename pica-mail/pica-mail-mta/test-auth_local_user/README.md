Cette image Docker a pour but de tester pica-mail-mta en se passant du LDAP.
Elle prend pica-mail-mta et rajoute :
* des adresses mails reconnues localement (inscrites dans la recipient table) : mail1@picasoft.net, mail2@picasoft.net
* des comptes d'utilisateur autorisés à utiliser ces adresses mail: mail1 et mail2 de mots de passe respectifs mail1pwd et mail2pwd autorisés à envoyer des mails respectivement en tant que mail1@picsoft.net et mail2@picasoft.net

Cette image docker est actuellement en développement. Pour le moment, nous sommes en train de travailler sur le premier point et pas sur le second, c'est-à-dire que n'importe qui peut se connecter au serveur et envoyer des mails en tant qu'une adresse de son choix, mais le destinataire doit être une adresse connue.

Le le réseau 127.0.0.1 a le droit de se connecter à la machine, mais pas besoin de mot de passe. On peut donc la tester en faisant
```
docker exec -it conteneur /bin/bash
apt update
apt install telnet
telnet localhost 25
helo pica01-test.test.picasoft.net
MAIL FROM:<mail1@picasoft.net>
RCPT TO:<mail2@picasoft.net>
DATA
Subject: bonjour
bla bla bla

.

```
Voilà!
