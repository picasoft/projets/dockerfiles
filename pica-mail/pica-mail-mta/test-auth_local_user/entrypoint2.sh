#!/bin/bash

/entrypoint.sh true

postfix stop
#génération du .db à partir du fichier texte
postmap /local_users
#ajout du .db en tant que source
postconf -e "virtual_mailbox_maps = hash:/local_users"

#on modifie temporairement les restrictions pour permettre à tout le monde d'envoyer du mail par notre serveur tant que la destination est autorisée (par les autres règles) et que celui qui tente de se servir du serveur est sur un réseau autorisé (typiquement l'hôte et le subnet)
postconf -e "mynetworks = 127.0.0.0/8"
postconf -e "smtpd_recipient_restrictions = permit_mynetworks, reject_unauth_destination"

#désactivation de la blacklist de clients à des fins de debug (elle rejette les clients en local)
postconf -e "smtpd_client_restrictions = permit_mynetworks"
#désactivation des restrictions sur le helo
postconf -e "smtpd_helo_restrictions = "

 #configuration du local delivery agent sous la forme du serveur lmtp de dovecot connecté par socket ip (inet)
 postconf -e "virtual_transport = lmtp:inet:${LMTP_LAN_HOSTNAME}:${LMTP_PORT}"
 #:private/dovecot-lmtp

postfix start
postfix reload
postfix stop

service rsyslog start

service postfix stop
service postfix start

tail -F /var/log/mail.log
