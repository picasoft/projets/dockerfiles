#!/bin/bash
/config.sh
service rsyslog start
service opendmarc start
service opendkim start
/usr/lib/postfix/sbin/master -c /etc/postfix -d 2>&1
