## Traefik

Ce dossier contient les ressources nécessaires pour lancer Traefik, un reverse proxy adapté pour Docker.
C'est la pièce la plus importante de l'infrastructure, puisque l'ensemble des communications HTTP(S) passent d'abord par Traefik, et il est aussi utilisé pour [générer des certificats pour les services TCP](../pica-tls-certs-monitor).

Ce service doit être lancé sur l'ensemble des machines de l'infrastructure.

Les explications sont sur le Wiki et doivent être modifiées en cas de changement majeur de configuration : https://wiki.picasoft.net/doku.php?id=technique:docker:general:traefik

### Configuration

La configuration a lieu dans les fichiers [traefik.toml](./traefik.toml) et [traefik_dynamic.toml](./traefik_dynamic.toml).
Notez que toute modification dans ce fichier impactera l'ensemble des machines, puisque le même fichier est utilisé pour l'ensemble des machines.

À des fins de tests, il peut être modifié localement sur les machines, mais doit toujours rester synchronisé avec ce dépôt à long terme.

Pour la génération des certificats, Traefik utilise Let's Encrypt. Il n'y a aucune configuration à faire de ce côté. Attention, le nombre de certificats générables est limité à 50 par semaine.

Si on lance plein de conteneurs de tests, on utilisera temporairement [l'environnement de qualification](https://letsencrypt.org/fr/docs/staging-environment/) de Let's Encrypt, en ajoutant la directive `caServer = "https://acme-staging-v02.api.letsencrypt.org/directory"` sous la section `[certificatesResolvers.letsencrypt.acme]`.

### Lancement

#### Certs

Au premier lancement, assurez-vous que :

- Le dossier `/DATA/docker/traefik/certs` existe
- Créez un fichier `acme.json` à l'intérieur
- Changez son propriétaire à `root`
- Changez ses permissions à `600`

C'est dans ce fichier que seront conservés tous les certificats générés par Traefik.

#### Métriques

Pour le bon fonctionnement des métriques, il faut aussi créer un fichier `.env` (dans le même dossier que le Docker Compose) qui devra contenir 2 variables :

- `SERVER_NAME` qui correspond au nom du serveur, par exemple `SERVER_NAME=pica01-test.picasoft.net`
- `METRICS_AUTH` qui correspond à la chaîne d'identification htpasswd utilisée pour authentifier sur l'endpoint des métriques, par exemple `METRICS_AUTH="traefik:$apr1$bXnknJ0S$GsC.ozNJc/dAkh9uH7Qlg."`

### Mise à jour

Il suffit de mettre à jour le tag de l'image dans Compose.
Lire la documentation [sur les mises à jour mineures](https://docs.traefik.io/v2.2/migration/v2/) pour voir s'il y a des opérations à effectuer ou des options dépréciées.

La mise à jour vers Traefik v2 a été effectuée ; quelques détails sont à consulter [sur le wiki](https://wiki.picasoft.net/doku.php?id=technique:adminsys:migration-traefik-v2).
