# Changelog

## v1.4 (17-06-2020)

* Fix crontab not scheduling because of instruction orders in Dockerfile

## v1.3 (05-04-2020)

* Use `python:3.8-alpine` as base image instead of `python:3-slim-buster` (it comes already with `cron`, so we don't have to install it)
* `pip`: don't use cache and add max version to avoid possible breaks with major version updates

## v1.2 (03-04-2020)

* Ajout d'un CHANGELOG
* CVE-2020-8492, CVE-2019-17455 et CVE-2019-18224 corrigées ([plus d'infos](/doc/mini_guide_cve.md))
* `getopt` remplacé par `argparse` ([plus d'infos](#getopt-et-argparse))
* Rotation des backups de pad.picasoft.net désactivée ([plus d'infos](https://team.picasoft.net/picasoft/pl/6brpbg7txpdmtxz7o4rhii5zuc))

## v1.1 (30-01-2020)

Cette version n'a jamais été explicitement définie dans le `docker-compose-yml`, mais correspond à la dernière version en prod en mars 2020. Les changements importants depuis la v1.0 semblent être les suivants (à vérifier avec Quentin):

* L'image de base du Dockerfile (`registry.picasoft.net/pica-debian:stretch
`) a été remplacée par une image Python officielle (`python:3-slim-buster`)
* Intégration de cette image à l'analyse de vulnérabilités de Clair
* [Remove localtime mount](https://gitlab.utc.fr/picasoft/projets/dockerfiles/-/commit/72be67797e451870e011d79263a90d050524d112)
* Divers autres correctifs

# Notes diverses

## `getopt` et `argparse`

Initialement, `fake_backups.py` utilisait le module `getopt` pour parser les arguments reçus. Cependant, `getopt` pose quelques problèmes:

* Exécuter juste `./fake_backups.py` ne déclenche pas une exception
* Si on lance `./fake_backups.py -n -f /tmp`, `getopt` va interpreter ça comme «une option `-n` avec un argument `-f` et un argument `/tmp`» au lieu de «une option `-n` sans argument et une option `-f` avec un argument `/tmp`», donc pas d'exception non plus

`getopt` a donc été remplacé par `argparse`, qui est beaucoup plus flexible et [facile à utiliser](https://docs.python.org/3.7/howto/argparse.html). `argparse` corrige les deux erreurs mentionnées précédemment et nous simplifie la vie en s'occupant lui même de faire des vérifications qui avant étaient implémentées dans `fake_backups.py` (par exemple, vérifier que l'argument de `-n` est un entier)

## Taille de l'image et vulnérabilités

* v1.3 (2020-04-06T00:09:32): 116MB, ? vulnérabilités (Clair n'affiche pas de warning)
* v1.2 (2020-04-05T19:15:14): 207MB, 60 vulnérabilités
* v1.1 (2020-01-30T17:14:31): 285MB, 74 vulnérabilités
