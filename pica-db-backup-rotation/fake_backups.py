#!/usr/bin/python3
from datetime import datetime, timedelta
import argparse
import os
import sys
import time

def main():

    parser = argparse.ArgumentParser(description="Generate dummy backup files, for debug purposes")
    parser.add_argument("-f", metavar="<folder>", help="destination folder", required=True)
    parser.add_argument("-n", metavar="<num_backups>", help="number of dummy backups to generate (default: %(default)s)", type=int, default=10)
    args = parser.parse_args()

    if not os.path.isdir(args.f):
        print('Please specify a valid path for argument -f <folder>.')
        sys.exit(2)

    i = 0
    # Creates args.n empty files named with the name of the service + the current date minus number of iterations
    start = time.time()
    while(i < args.n):
        d = datetime.today() - timedelta(hours=i)
        filename = d.strftime('%Y.%m.%d.%H%M%S')
        file = open(args.f + '/' + filename + '.tar.gz', 'w')
        file.close()
        i += 1
    end = time.time()
    time_elapsed = end - start

    print('[' + args.f + '/]')
    # Displays the time needed to generate all files
    print(str(i) + ' files generated in ' + str(time_elapsed) + ' seconds.')

if __name__ == "__main__":
    main()
