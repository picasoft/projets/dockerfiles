#!/usr/bin/env python3
import os
import sys
import json
from crontab import CronTab

# Setting constants
BACKUP_FOLDER = '/backup'
CONFIG_FOLDER = '/config'
CONFIG_FILE = 'backup_config.json'
PATH_TO_CONFIG = CONFIG_FOLDER + '/' + CONFIG_FILE

# Checks if the config file and folders can be accessed
if not(os.path.exists(BACKUP_FOLDER) and os.path.exists(CONFIG_FOLDER)):
    sys.stderr.write('Err: BACKUP_FOLDER or CONFIG_FOLDER doesn\'t exist. \n')
    sys.exit()

if not(os.path.isfile(PATH_TO_CONFIG)):
    sys.stderr.write('Err: ' + PATH_TO_CONFIG +  ' not found. \n')
    sys.exit()

# Creates a cron table
cron = CronTab(user="root")
cron.remove_all()

print("Building configuration file for backup rotation...\n")
# Pulls information about the services from the config file
try:
    with open(PATH_TO_CONFIG) as json_file:
        services_list = json.load(json_file)

        #For each service in the config file, creates a new cron job
        for service in services_list:
            if os.path.isdir(os.path.join(BACKUP_FOLDER, services_list[service]["Folder"])):
                backup_rota = services_list[service]["Backup-Rota"]
                rotation_cmd = "/usr/local/bin/rotate-backups --syslog=true -H " +str(backup_rota["Hour"]) + " -d " + \
                    str(backup_rota["Day"]) + " -w " + str(backup_rota["Week"]) + " -m " + str(backup_rota["Month"]) + " " + \
                        BACKUP_FOLDER + "/" + services_list[service]["Folder"]
                job = cron.new(command=rotation_cmd, user = 'root')
                job.setall("0 0 * * *")
                if job.is_valid():
                    job.enable()
                else:
                    sys.stderr.write('Err: syntax error in ' + CONFIG_FILE)
                    sys.exit()
                print("Info : Folder {0} scheduled for rotation".format(services_list[service]["Folder"]))
            else:
                print("Warning : Folder {0} does not exist, skipping...".format(services_list[service]["Folder"]))
        print("\nConfiguration done. Cron jobs enabled : ")
        for job in cron:
            print("\t{0}".format(job))
except:
    sys.stderr.write('Err: while opening JSON config file.\n')

# Write the cron jobs to the system crontab
cron.write("/crontab.conf")
