## Picablog

Ce dossier contient les fichiers nécessaires pour lancer une instance de Plume sur les serveurs de Picasoft.

Nous nous basons sur l'image officielle car le [Dockerfile](https://github.com/Plume-org/Plume/blob/master/Dockerfile) est bien écrit et léger.

En plus, nous ajoutons :

* Un système d'initialisation directement dans l'image (plutôt que d'avoir [à lancer des commandes manuellement](https://docs.joinplu.me/installation/with/docker))
* La détection de la mise à jour de l'image pour lancer les migrations
* Un entrypoint permettant d'attendre que le serveur de base de données soit prêt
* Des variables d'environnement qui ne devraient pas changer directement dans le Dockerfile
* Un `HEALTHCHECK`

### Mise à jour

Mettre à jour `VERSION` **et** `PLUME_VERSION` dans le [Dockerfile](./Dockerfile) et ajuster le tag de l'image construite dans le [docker-compose.yml](./docker-compose.yml)

### Configuration et lancement

Copier le fichier `plume.secrets.example` dans `plume.secrets` et `plume_db.secrets.example` dans `plume_db.secrets` et remplacez les valeurs par des mots de passe de production.

Lancer :

```bash
docker-compose up -d
```

### Administration de l'instance

```bash
docker exec -it plume
```

Puis utilisation de l'[outil plm](https://docs.joinplu.me/CLI/), par exemple pour créer de nouveaux utilisateurs sur une instance privée.

### Évolution de l'image

On pourra vérifier les variables configurables dans Plume [sur le fichier d'exemple officiel](https://github.com/Plume-org/Plume/blob/master/.env.sample) (à adapter à la version courante).

Pour créer une deuxième instance, il suffira de rajouter un deuxième `plm instance new` dans le fichier [entrypoint.sh](./entrypoint.sh) et d'ajouter un `Host` Traefik dans le Docker Compose. Ainsi, on pourrait avoir une instance privée, où seul le compte Picasoft publie (`--private`), et une instance publique, pour tout le monde.

### Documentation générale

Vous pouvez trouver la documentation officielle ici : https://docs.joinplu.me/
