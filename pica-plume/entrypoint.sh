#!/bin/bash

set -e
# Checks if Plume has already been launched one
# Otherwise, initialize the instance and create
# a file at FIRSTLAUNCH_PATH to indicate that the
# instance has already been initialized
# FIRSTLAUNCH_PATH is configured via environment
#
# Also manage running migrations when updating

if [ -z "${POSTGRES_PASSWORD}" ]; then
	echo >&2 'Error : missing required ${POSTGRES_PASSWORD} environment variable, exiting.'
	exit 1
fi
if [ -z "${POSTGRES_USER}" ]; then
	echo >&2 'Error : missing required ${POSTGRES_USER} environment variable, exiting.'
	exit 1
fi
if [ -z "${POSTGRES_DB}" ]; then
	echo >&2 'Error : missing required ${POSTGRES_DB} environment variable, exiting.'
	exit 1
fi

export PGPASSWORD="${POSTGRES_PASSWORD}"
# Wait for database to be ready
while ! psql -h"${DB_HOST}" -U"${POSTGRES_USER}" -d"${POSTGRES_DB}" -c "SELECT 1" &>/dev/null; do
  echo "Database server not ready yet, re-trying in 5 seconds..."
  sleep 5
done

# Create the .env file used by Plume
echo "Create environment file for Plume..."
echo "DATABASE_URL=postgres://${POSTGRES_USER}:${POSTGRES_PASSWORD}@${DB_HOST}:5432/${POSTGRES_DB}" >> /app/.env

# If first launch, initialize and create marker file
if [ ! -f ${FIRSTLAUNCH_PATH} ]; then
  echo "First launch detected."
	echo "Running migrations..."
	plm migration run
  echo "Initialize search index..."
  plm search init
  echo "Initialize instance..."
  plm instance new -d "$BASE_URL" -n "$NAME" -l "CC-BY-SA" --private
  echo "Create admin user..."
	# Todo bio
  plm users new -n "$ADMIN_USER" -N "$ADMIN_NAME" -b "L'association Picasoft a pour objet de promouvoir et défendre une approche libriste, inclusive, respectueuse de la vie privée, respectueuse de la liberté d'expression, respectueuse de la solidarité entre les humains et respectueuse de l'environnement, notamment dans le domaine de l'informatique. Plus d'informations sur [picasoft.net](https://picasoft.net). Vous pouvez également nous suivre [sur Mastodon](https://mamot.fr/@picasoft) et nous contacter à l'adresse picasoft@assos.utc.fr." -e "$ADMIN_EMAIL" -p "$ADMIN_PASS" --admin
  echo "Done."
  touch ${FIRSTLAUNCH_PATH}
fi

# Check if we updated since last launch
if [ "${PLUME_VERSION}" != "$(cat ${FIRSTLAUNCH_PATH})" ]; then
   # If so, we need to run migrations
   echo "Instance updated since last launch, running migrations..."
   plm migration run
fi

# Now write the current version into the file
echo "${PLUME_VERSION}" > ${FIRSTLAUNCH_PATH}

echo "Launching Plume..."
exec "$@"
