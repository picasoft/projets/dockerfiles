# Dockerfiles de Picasoft

Ce dépôt centralise les Dockerfiles et autre ressources utilisées pour construire **et** déployer les images Docker tournant en production sur l'infrastructure de Picasoft.

La documentation pour comprendre et utiliser ce dépôt se trouve sur [la section du Wiki dédiée à Docker](https://wiki.picasoft.net/doku.php?id=technique:docker:picasoft:start).
Chaque sous-dossier contient les instructions spécifiques à chaque service, mais ne rappelle pas toutes les étapes (construction, test, etc...).

Les README de ce dépôt ne concernent que le déploiement, la mise à jour, etc.
Pour l'utilisation et l'administration, voir [la section du Wiki dédiée aux services](https://wiki.picasoft.net/doku.php?id=technique:adminserv:start).

**Il est important de mettre à jour le Wiki en cas de modification majeure sur ce dépôt ou de la façon de gérer les services. :D**
