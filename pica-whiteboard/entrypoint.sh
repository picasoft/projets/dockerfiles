#! /bin/sh

cd /opt/whiteboard
cat << EOF > crontab
$STOP_APP pkill node
$RESTART_APP cd /opt/whiteboard && node scripts/server.js --mode=production
EOF
node scripts/server.js --mode=production &
nginx
supercronic crontab
