# uMap

Ce dossier permet de déployer une instance de [uMap](https://github.com/umap-project/umap/), un logiciel libre permettant de créer des cartes personnalisées sur fond de carte OpenStreetMap.

Cette image Docker essentiellement utilise le package Python `umap-project`, et ajoute :
* `uWSGI`, utilisé ici comme serveur d'application Python.
* Un entrypoint permettant d'initialiser la base de donnée et de compresser les fichiers statiques.

Lors du lancement de l'application, les fichiers statiques sont copiés dans `/srv/umap/static`. Le fichier [uwsgi.ini](./uwsgi.ini) permet justement de router les requêtes qui commencent par `/static` vers ce dossier.

Seuls les utilisateurs authentifiés peuvent créer une carte. L'enregistrement de nouveaux utilisateurs n'est pas autorisé.

Attention : par défaut, les cartes sont **publiques** : il faudra, pour chaque carte, rendre la carte [uniquement accessible aux éditeurs](https://wiki.openstreetmap.org/wiki/UMap/FAQ#How_to_make_a_.22private.22_uMap.3F).

## Configuration

L'ensemble de la configuration se fait via l'environnement.
Le fichier [settings.py](./settings.py) est injecté au lancement de `uMap` (via `UMAP_SETTINGS`), et permet de charger toutes les variables de configuration à partir des variables d'environnement, ou des valeurs par défaut.

Les variables sont réparties entre le fichier Compose et les fichiers de secrets.

## Lancement

S'assurer que l'image a été construite et poussée sur le registre de production et que les fichiers `.secrets` sont créés.

```bash
docker-compose up -d && docker-compose logs -f
```

L'interface d'administration est accessible à https://carte.caretech.picasoft.net/admin.

### Premier lancement

#### Création de l'administrateur

Dans la configuration actuelle, l'instance est fermée et les comptes doivent être créés manuellement.

En particulier, un compte administrateur doit être créé au premier lancement.

```bash
$ docker-compose exec app bash
$ umap createsuperuser
# Example
Loaded local config from /srv/umap/settings.py
Utilisateur: caretech
Adresse électronique: caretech@xxx
Password:
Password (again):
Superuser created successfully.
```

Remplir les différents champs.

#### Configuration des fonds de carte

On se rend dans l'interface d'administration (https://carte.caretech.picasoft.net/admin/), puis dans `Tile Layers`, et on choisit d'en ajouter un.

Les URL à ajouter pour les fonds de carte peuvent être trouvées ici : https://wiki.openstreetmap.org/wiki/Tile_servers

Par exemple, on ajoutera OpenStreetMap-FR et on lui donnera une priorité plus élevée que le fond de carte présent par défaut (Positron) : c'est vraisemblablement lui qui sera utilisé :

* URL : `https://{s}.tile.openstreetmap.fr/osmfr/{z}/{x}/{y}.png`
* Attribution : `© Openstreetmap France | Données © [[https://www.openstreetmap.org/copyright|les contributeurs OpenStreetMap]]`
* Rang : 1

On peut ensuite naviguer entre les deux fonds de carte.

## Mise à jour

Changer l'argument `UMAP_VERSION` du [Dockerfile](./Dockerfile), et mettre à jour le tag de l'image dans le fichier Compose.

Pour les mises à jour majeures des SGBD, voir leur documentation officielle.
