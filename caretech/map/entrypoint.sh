#!/usr/bin/env bash

# default variables
: "${SLEEP:=1}"
: "${TRIES:=60}"

function wait_for_database {
  echo "Waiting for database to respond..."
  tries=0
  while true; do
    [[ $tries -lt $TRIES ]] || return
    (echo "from django.db import connection; connection.connect()" | umap shell)
    [[ $? -eq 0 ]] && return
    sleep $SLEEP
    tries=$((tries + 1))
  done
}

echo "Wait for the database..."
wait_for_database
echo "Migrate the database..."
umap migrate
echo "Collect static files..."
umap collectstatic --noinput
echo "Compress static files..."
umap compress

echo "Launch uMap via uWSGI..."
exec $@
