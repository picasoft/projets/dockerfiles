# Wiki.js

Ce dossier permet de déployer une instance de [Wiki.js](https://wiki.js.org/).

## Configuration

La configuration se fait via les variables d'environnement et le fichier de secrets, et ne concerne que la connexion à la base de données dans notre cas.
Le reste de la configuration (utilisateurs, permissions...) est géré dynamiquement depuis l'interface.

Copier `db.secrets.example` à `db.secrets` et remplacer les valeurs.

## Lancement

Lancer `docker-compose up -d && docker-compose logs -f` et vérifier qu'il n'y a pas d'erreurs.

## Premier lancement

Lors du premier lancement, en consultant le site, on arrivera sur une page permettant de finaliser l'installation.

En particulier, on va créer un compte administrateur.

On choisit un email et un mot de passe et on remplit l'URL du wiki (celle par laquelle on est arrivé sur la page). L'email n'a pas besoin d'exister pour le compte administrateur : il fait office de login.

On désactive la télémétrie.

On arrive ensuite sur une page permettant de créer la page d'accueil du wiki.

À partir d'ici, on configurera ce que l'on veut dans l'interface d'administration (https://wiki.caretech.picasoft.net/a) : thème, permissions, langue... On se réferera à la documentation officielle.

Par défaut, seul l'administrateur pourra se connecter, les inscriptions ne sont pas ouvertes.

## Mise à jour

Mettre à jour le tag dans le fichier Compose et relancer le service. Aucune autre action n'est nécessaire. Attention lors des mises à jour majeures de Postgres (*e.g.* 12 → 13), voir la documentation officielle.
