# Serveur LDAP

<!-- TOC depthFrom:2 depthTo:6 withLinks:1 updateOnSave:1 orderedList:0 -->

- [Démarrage](#dmarrage)
- [Configuration](#configuration)
	- [Schémas additionnels](#schmas-additionnels)
	- [Entrée initiales de l'annuaire](#entre-initiales-de-lannuaire)
	- [Configuration par défaut](#configuration-par-dfaut)
	- [Certificats](#certificats)
	- [Secrets](#secrets)
	- [Plus de logs](#plus-de-logs)
- [Mise à jour de l'image](#mise-jour-de-limage)
- [Démarrage du conteneur](#dmarrage-du-conteneur)
- [Test de l'image sur un client](#test-de-limage-sur-un-client)

<!-- /TOC -->

Cette image est basée sur [osixia/openldap](https://github.com/osixia/docker-openldap). Elle est spécialisée pour les besoins de l'infrastructure de Picasoft.

Cette image **doit être lancée** aux côtés de [TLS Certs Monitor](../pica-tls-certs-monitor) pour fonctionner.

Tout changement de structure doit être [documenté sur le wiki](https://wiki.picasoft.net/doku.php?id=technique:adminsys:ldap:start).

## Démarrage

Copier `pica-openldap.secrets.example` en `pica-openldap.secrets`, puis lancer :

```bash
docker-compose up -d && docker-compose logs -f
```

Notez que l'ensemble de la configuration pour préparer l'instance n'est utilisée que lors du premier lancement : elle n'aura aucun effet sur les instances existantes.

Lors du premier lancement, on vérifiera s'il y a [des permissions à rajouter à la main](https://wiki.picasoft.net/doku.php?id=technique:adminsys:ldap:acl).

## Configuration

Par rapport à la configuration de base, cette version :
* Spécifie l'organisation Picasoft (variables d'environnement dans le Docker Compose),
* Configure un utilisateur `readonly` appellé `nss`,
* Active et force la connexion TLS au serveur LDAP,
* Ajoute les schémas additionnels (permettant d'utiliser `host` et `authorizedService`, par exemple),
* Crée la structure du base du LDAP (Group, People, Service),
* Ajoute des entrées d'exemple pour les comptes.

La configuration par défaut de l'image est dans les répertoires `bootstrap`, `environment` et `secrets`.

La table suivante donne le rôle de ces répertoires :

| Répertoire | Contenu | À quoi ça sert |
|------------|---------|----------------|
| [bootstrap/schema](./bootstap/schema) | Schémas additionnels (format ldif) | Schémas ajoutés automatiquement à l'initialisation de la configuration |
| [bootstrap/ldif](./bootstrap/ldif) | Entrées initiales de l'annuaire (format ldif) | Entrées automatiquement ajoutées à l'initialisation de l'annuaire |
| [environment](./environment) | Configuration par défaut | Définition des valeurs par défaut des variables d'environnement utilisables dans le Docker Compose |
| [secrets](./secrets) | Mots de passe | Mots de passe utilisés pour les trois utilisateurs par défaut (admin, config et nss)

Le contenu de ces répertoire est copié dans l'image à la construction. Cette copie
est supprimée par défaut une fois l'image configurée, après la première exécution du
conteneur.

### Schémas additionnels

Le fichier [ldapns.schema](./bootstrap/schema/ldapns.schema) permet le support des attributs `host` et `authorizedServices`.

### Entrée initiales de l'annuaire

Le fichier [init.ldif](./bootstrap/ldfi/init.ldif) crée la structure de base de l'annuaire :
* Une OU (Organizational Unit) `People`, pour les comptes POSIX personnels,
* Une OU `Groups`, pour les groupes POSIX,
* Une OU `Services`, pour les comptes POSIX "virtuels", destinés aux services comme Mattermost.

Il crée aussi des entrées `example` donnant un exemple pour chacun de ces types.

### Configuration par défaut

* Le fichier [pica.startup.yaml](./environment/pica.startup.yaml) contient toutes les
valeurs par défaut des paramètres de configuration. Ces valeurs sont utilisées à la
création de le configuration du serveur ldap **à la première exécution du conteneur**. Le fichier est supprimé ensuite.

* Le fichier [pica.yaml](./environment/pica.yaml) contient les paramètres de
de configuration utilisés à chaque démarrage du conteneur. Pour l'instant il est utilisé pour définir le niveau de messages de débogage de `slapd` et limiter le nombre
de descripteurs de fichiers utilisés (1024). On peut l'augmenter si on estime qu'il y aura plus de connexions simultanées sur le serveur.

### Certificats

La configuration par défaut spécifie les fichiers suivants :

| Fichier | Rôle |
|---------|------|
| cert.pem | Certificat serveur |
| chain.pem | Certificat CA |
| privkey.pem | Clé privée serveur |

### Secrets

Le repertoire [secrets](./secrets) doit contenir le fichier `pica-openldap.secrets`
avec les mots de passe en clair qui seront utilisés pour l'administrateur de
l'annuaire, l'accès à la configuration et l'accès en lecture seule. Un modèle est
fourni dans le fichier [pica-ldap.secrets.examples](./secrets/
pica-ldap.secrets.example).

| Variable | Usage |
|----------|-------|
| LDAP_ADMIN_PASSWORD | Mot de passe de l'adminsitrateur de l'annuaire (cn=admin,dc=picasoft,dc=net)|
| LDAP_CONFIG_PASSWORD | Mot de passe de configuration (cn=admin,cn=config) |
| LDAP_READONLY_USER_PASSWORD | Mot de passe de l'utilisateur *read only* (cn=nss,dc=picasoft,dc=net) |

> Attention : les mots de passe apparaîtront **en clair** dans l'environnement du conteneur.

### Plus de logs

Pour obtenir plus de logs, il suffit d'ajouter la ligne suivante au service `ldap-host` dans le fichier
[docker-compose.yml](./docker-compose.yml) :

```yaml
command: --loglevel debug
```

## Mise à jour de l'image

Il suffit de modifier la version de osixia/openldap dans le [Dockerfile](./Dockerfile) :

```Dockerfile
FROM osixia/openldap:XXX
```

Ne pas oublier de mettre à jour la version dans le [docker-compose.yml](./docker-compose.yml) :

```
image: registry.picasoft.net/pica-openldap:XXX
```

## Test de l'image sur un client

* Accès à la configuration (utiliser le mot de passe défini par `LDAP_CONFIG_PASSWORD`)

```bash
ldapsearch -Z -W -x -D cn=admin,cn=config -b cn=config
```

* Accès à l'annuaire (utiliser le mot de passe défini par `LDAP_ADMIN_PASSWORD`)

```bash
ldapsearch -Z -W -x -D cn=admin,dc=picasoft,dc=net -b dc=picasoft,dc=net
```

Ces deux commandes doivent fonctionner **dans le conteneur**. Si ce n'est pas le cas, il y
a une erreur de configuration.
