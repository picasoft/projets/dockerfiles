## NextCloud Compiègne en Transition

### Personnalisation

Le thème se configure directement via les paramètres de l'application, en tant qu'administrateur.
Pour ce faire, se rendre dans Paramètres → Personnaliser l'apparence.

Le fichier [logo.png](./logo.png) est utilisé pour le Logo et le Logo d'en-tête.
Le fichier  [favicon.png](./favicon.png) est utilisé pour le Favicon.

Les CGU sont celles de Picasoft : https://picasoft.net/co/cgu.html

La couleur de fond est `#8CBD8C`.

### Mise à jour de PostgreSQL

Il peut arriver que la version de PostgreSQL ne soit plus supportée par NextCloud.
Sans en arriver là, il est bon de régulièrement mettre à jour PostgreSQL :
> While upgrading will always contain some level of risk, PostgreSQL minor releases fix only frequently-encountered bugs, security issues, and data corruption problems to reduce the risk associated with upgrading. For minor releases, the community considers not upgrading to be riskier than upgrading. https://www.postgresql.org/support/versioning/

Les mise à jours mineures (changement du Y de la version X.Y) peuvent se faire sans intervention humaine. On veillera à bien regarder les logs.

En revanche, le passage d'une version majeure à une autre nécessitera une intervention manuelle.

La documentation complète est ici : https://www.postgresql.org/docs/current/upgrading.html

De manière générale, la façon la plus simple est de se rendre dans l'ancien conteneur, de réaliser un `pg_dumpall` et de le copier en lieu sûr (`docker cp`).
Ensuite, on supprime l'ancien volume de base de données, on relance le nouveau conteneur de base de données (qui sera sans donnée), on monte le fichier de dump, et on lance un `psql -U <user> -d <db> -f <dump_file>` (valeurs de `user` et `db` à matcher avec le fichiers de secrets).

On attend, et **si tout s'est bien passé**, on peut lancer le conteneur applicatif (NextCloud).
