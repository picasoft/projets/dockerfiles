## NextCloud Picasoft

### Mise à jour de MariaDB

[Selon la documentation](https://mariadb.com/kb/en/upgrading-between-major-mariadb-versions/) :

> MariaDB is designed to allow easy upgrades. You should be able to trivially upgrade from ANY earlier MariaDB version to the latest one (for example MariaDB 5.5.x to MariaDB 10.5.x), usually in a few seconds.

L'idée est d'éteindre le conteneur applicatif (NextCloud), puis de lancer la nouvelle version du conteneur, d'entrer dedans, de lancer la commande `mysql_upgrade` et de redémarrer le conteneur.
