## NextCloud

Ce dossier contient les ressources nécessaires pour lancer une ou plusieurs instances NextCloud.

Deux instances sont gérées : celle de Picasoft et celle de Compiègne en Transition.
Elles utilisent l'image officielle de NextCloud.

Voir les README des dossiers pour la configuration spécifique.

### Configuration

Quasiment aucune configuration n'est effectuée via les fichiers de ce dépôt, et on préfère l'interface web.
Le défaut est qu'il n'est pas possible de lancer des instances NextCloud **vraiment** personnalisées depuis ce dépôt, mais c'est parce que le format des fichiers de configuration est amené à évoluer et que NextCloud effectue des migrations automatiques lors des mises à jour.

Versionner les fichiers de configuration serait donc en conflit avec les modifications automatiques effectuées par NextCloud lors des mises à jour et des changements dans l'interface.

Les fichiers `nginx.conf` sont repris de [cet exemple](https://github.com/nextcloud/docker/blob/master/.examples/docker-compose/with-nginx-proxy/postgres/fpm/web/nginx.conf).

### Lancement

Pour l'instance de Compiègne en Transition, se rendre dans le dossier [cet](./cet).
Pour l'instance de Picasoft, se rendre dans le dossier [pica](./pica).

Copier les fichiers `.secrets.example` en `.secrets` et remplacer les valeurs.
Lancer `docker-compose up -d`.

### Mise à jour

Pour mettre à jour l'instance de Picasoft, il suffit de mettre à jour le tag de l'image officielle de NextCloud.

Attention : **toutes les mises à jour de version majeure doivent se faire une par une**. Les logs applicatifs détaillent la mise à jour.
Exemple :
* 15 -> 16, puis
* 16 -> 17, puis
* 17 -> 18.

Sinon, il y a risque de casse.
