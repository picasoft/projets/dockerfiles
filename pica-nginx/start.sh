#!/bin/bash

set -e

if [ "$AUTOINDEX" = "true" ] && [[ `cat /etc/nginx/nginx.conf | grep autoindex` == "" ]]; then
	sed -i '/index index.php index.html index.htm;/a \        autoindex on;' /etc/nginx/nginx.conf
fi

chown -R www-data /var/www
mkdir -p /var/run/php
exec /usr/bin/supervisord -c /etc/supervisord.conf
