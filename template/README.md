## App

In this README, you should explain, if applicable, the following :
* What is this
* Is it based on a base image, is it a custom Dockerfile based on another Dockerfile, or a brand new Dockerfile
* How to configure (secrets, environment variables...)
* How to start (usually just a `docker-compose up -d` and copying the secret files : this is the goal)
* How to update the service itself (usually just changing a tag in the Docker Compose file and an argument in the Dockerfile)
* How to update the customization of the service : add more environment variables, change configuration, etc.
* How to administrate the service (e.g. CLI tool)
* Warnings about breaking changes (e.g. "you cannot update the database to a major version without doing this or that")

And everything that you find useful.

If you use a single folder for multiple Dockerfiles, *e.g.* 2, you can either :
* Create two subfolder containing a Dockerfile
* Or create two Dockerfile, with the name `<service>.Dockerfile`

For example, the mail service uses two Dockerfile : one for the MTA, one for the MDA.

This README should act as a reference for all administration tasks.

However it should not contain user documentation, nor general advices about how to resolve build errors and so on (please use the Wiki).
