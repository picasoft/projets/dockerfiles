#!/usr/bin/env python3

"""Prometheus exporter for Lufi."""

# Imports
import os
import sys
import time
import signal
from urllib.parse import urlparse
from prometheus_client import start_http_server, Gauge, REGISTRY, PROCESS_COLLECTOR, PLATFORM_COLLECTOR
import requests


def get_peertube_stats(url):
    """
    Get statistics from Peertube
    :param url: Peertube base URL
    :returns: Peertube JSON raw statistics
    """
    # Get Peertube metrics
    stats = requests.get(url + '/api/v1/server/stats').json()
    return stats


def exit_handler(sig, frame):
    """
    Exit script properly
    """
    print('Terminating...')
    sys.exit(0)


def main():
    """Main function"""
    # Number of seconds between 2 metrics collection
    collect_interval = int(os.getenv('EXPORTER_COLLECT_INTERVAL', "60"))
    # Port for metrics server
    exporter_port = 8000
    # Get instance URL
    instance_url = os.getenv("INSTANCE_URL", "https://tube.picasoft.net")
    instance_name = urlparse(instance_url).netloc

    # Remove unwanted Prometheus metrics
    [REGISTRY.unregister(c) for c in [
        PROCESS_COLLECTOR,
        PLATFORM_COLLECTOR,
        REGISTRY._names_to_collectors['python_gc_objects_collected_total']
    ]]

    # Start Prometheus exporter server
    start_http_server(exporter_port)

    # Register metrics
    videos_gauge = Gauge(
        'peertube_videos_total',
        'Number of videos locally and federated',
        ['instance_name', 'scope']
    )
    videos_views_gauge = Gauge(
        'peertube_views_total',
        'Number of views for videos of this instance',
        ['instance_name']
    )
    videos_size_gauge = Gauge(
        'peertube_videos_size_total',
        'Size of all local videos files of this instance',
        ['instance_name']
    )
    comments_gauge = Gauge(
        'peertube_comments_total',
        'Number of local and federated comments for videos on this instance',
        ['instance_name', 'scope']
    )
    users_gauge = Gauge(
        'peertube_users_total',
        'Number of users accounts on the instance',
        ['instance_name']
    )
    followers_gauge = Gauge(
        'peertube_followers_total',
        'Number of followers of this instance',
        ['instance_name']
    )
    following_gauge = Gauge(
        'peertube_following_total',
        'Number of followed instances',
        ['instance_name']
    )

    # Loop forever
    while True:
        # Get Peertube statistics
        metrics = get_peertube_stats(instance_url)

        # Update metrics
        videos_gauge.labels(instance_name=instance_name, scope='local').set(metrics['totalLocalVideos'])
        videos_gauge.labels(instance_name=instance_name, scope='federation').set(metrics['totalVideos'])
        videos_views_gauge.labels(instance_name=instance_name).set(metrics['totalLocalVideoViews'])
        videos_size_gauge.labels(instance_name=instance_name).set(metrics['totalLocalVideoFilesSize'])
        comments_gauge.labels(instance_name=instance_name, scope='local').set(metrics['totalLocalVideoComments'])
        comments_gauge.labels(instance_name=instance_name, scope='federation').set(metrics['totalVideoComments'])
        users_gauge.labels(instance_name=instance_name).set(metrics['totalUsers'])
        followers_gauge.labels(instance_name=instance_name).set(metrics['totalInstanceFollowers'])
        following_gauge.labels(instance_name=instance_name).set(metrics['totalInstanceFollowing'])

        # Wait before next metrics collection
        time.sleep(collect_interval)


if __name__ == '__main__':
    # Catch SIGINT and SIGTERM signals
    signal.signal(signal.SIGINT, exit_handler)
    signal.signal(signal.SIGTERM, exit_handler)
    main()
