## Peertube

Ce dossier contient les fichiers nécessaires pour monter une instance de [PeerTube](https://joinpeertube.org), un service d'hébergement de vidéos libre et fédéré.

## Configuration

Copier les fichiers de secret en enlevant le `.example` et en mettant à jour les variables dedans avec les bons secrets.

## Lancement

Simplement lancer le fichier compose.

Au premier lancement :
- L'initialisation est automatique.
- Elle échoue si PeerTube n'arrive pas à se connecter au serveur SMTP.
- Le mot de passe root est dans les logs !

## Mise à jour

Pour l'instant (au 12/11/2020), l'image Docker officielle n'a pas de tags de versions à jour.
Le seul tag à jour est un *rolling* tag, `production-buster`.
À suivre !
