## pica-dokuwiki:hogfather-patch1

* Activation du plugin `mbstring` de PHP
* Réglage de la Timezone de PHP sur Europe/Paris
* Réglage de la locale sur `fr_FR.UTF-8`

## pica-dokuwiki:hogfather

Passage à la nouvelle version stable : `Release 2020-07-29 “Hogfather”`
