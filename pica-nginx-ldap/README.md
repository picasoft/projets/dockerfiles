## nginx with LDAP authentication

nginx has a mechanism allowing to use an external service handling authentication requests. For LDAP, the traditionnal way involves running a [ldap-auth](https://github.com/nginxinc/nginx-ldap-auth) daemon. It seemed a bit burdensome to me, even if there is a [Docker image](https://hub.docker.com/r/bitnami/nginx-ldap-auth-daemon/) for the daemon. I think this is because LDAP auth is included in NGINX Plus and a bit tricky to integrate with free pre-built packages.

There is an unofficial nginx module, [nginx-auth-ldap](https://github.com/kvspb/nginx-auth-ldap), which works well. We just need to recompile nginx with it, and voilà.

This is the purpose of this repository : make a ready-to-use nginx single image with LDAP auth.

## Sample usage with Compose

```yaml
version: '3.8'

services:
  nginx_ldap:
    image: registry.picasoft.net/pica-nginx-ldap:1.21.4
    container_name: registry_frontend
    environment:
      LDAP_URL: ldaps://ldap.picasoft.net:636
      LDAP_BASE_DN: dc=picasoft,dc=net
      LDAP_ANSWER_ATTRIBUTES: cn
      LDAP_SCOPE_SEARCH: sub
      LDAP_FILTER: (objectClass=posixAccount)
      LDAP_BIND_DN: cn=nss,dc=picasoft,dc=net
      SERVER_NAME: registry.picasoft.net
    ports:
      - 8080:80
    env_file: ./secrets/ldap.secrets
    restart: unless-stopped
```

With `LDAP_BIND_PASSWORD=XXX` in `ldap.secrets`.

## Build

```
docker build -t nginx-ldap .
```

## LDAP configuration

Via Environments variables :

- `LDAP_URL` → `ldap[s]://<url>:<port>`
- `LDAP_BASE_DN` → where to start the search
- `LDAP_ANSWER_ATTRIBUTES` → which attributes to get back, comma-separated.
- `LDAP_SCOPE_SEARCH` : `base`, `one` or `sub`
- `LDAP_FILTER` : constrain the search
- `LDAP_BIND_DN` : user with read access
- `LDAP_BIND_PASSWORD` : password for the user with read access

See [official docs](https://ldapwiki.com/wiki/LDAP%20URL) for detail.

## Server configuration

Environment variable :

- `SERVER_NAME` : URL of your website.

Mount a file at `/etc/nginx/site.conf`. Every line will be included in the `server {}` section of `nginx.conf`.

Environment variables **won't be** subtituted at startup.

Note that nginx always listen internally on port 80. To change all configuration you can just mount an `nginx.conf` file. The moninal use is pretty standard : act as an authorization proxy or serve a simple single website.
