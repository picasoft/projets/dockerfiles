#!/bin/sh

# Substitute env variables (secrets) in nginx configuration
envsubst < /etc/nginx/templates/nginx.conf.template > /etc/nginx/nginx.conf

# Execute initial command
exec $@
