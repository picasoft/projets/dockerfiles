## Filter et faire suivre les hooks Wekan

Wekan a une fonctionnalité pour [déclencher un *hook*](https://github.com/wekan/wekan/pull/1119) lors de l'activité sur un board.
Grâce aux hooks, on peut configurer Mattermost pour recevoir un message sur un canal donné dès qu'une activité est détectée.

Le souci est que Wekan [déclenche les hooks sortants pour chaque activité](https://github.com/wekan/wekan/wiki/Webhook-data), même les plus insignifiantes, ce qui spamme le canal.

L'idée est donc de passer par un intermédiaire pour filter les hooks selon le type d'activité, et ne transmettre que les activités intéressantes. C'est ce que fait ce petit outil. Le code est très basique et pourra être amélioré.

### Lancement

Il suffit de construire l'image et de lancer l'outil.
On peut aussi le faire à la main :
```
gunicorn --bind 0.0.0.0:5000 --reload main:app
```

### Utilisation

Supposons que dans une application type Mattermost, on configure un hook **entrant** qui peut recevoir des requêtes `POST` de Wekan. Cette URL est désignée par `URL_HOOK`.

Cet outil est déployé dans le même réseau que le Wekan. Son nom de conteneur est `wekan-filter-hooks`.

Dans Wekan, l'URL du hook sortant sera alors (à la place de `URL_HOOK`) :
```
http://wekan-filter-hooks:5000/forward_hooks?url=URL_HOOK
```

Ainsi, cet outil n'est appelable que depuis Wekan (car le nom d'hôte n'est résolu que dans son réseau), ce qui permet d'éviter un spam extérieur.

Quand il reçoit une requête, l'outil examine le hook et voit si le type d'activité correspond à une liste pré-définie et l'envoie à `URL_HOOK` le cas échéant.

### Type d'activité pris en compte

Les hooks "pertinents" sont définis directement [dans le code](./main.py).
