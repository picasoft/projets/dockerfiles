#!/usr/bin/env python
# -*- coding: utf-8 -*-

import json
import logging

import requests

from flask import Flask, request

# Configure logging
log_format = '%(asctime)s [%(levelname)s] ' \
             '%(message)s (%(filename)s:%(lineno)d)'
logging.basicConfig(
    format=log_format,
    datefmt='%Y/%m/%d %H:%M:%S',
    level=logging.INFO
)

# Run Flask
app = Flask(__name__)

# List of hooks to forward to the real hook URL
# See https://github.com/wekan/wekan/wiki/Webhook-data
allowed_hooks = [
    'act-createCard',
    'act-moveCard',
    'act-addComment'
]

@app.route('/forward_hooks', methods=['POST'])
def filter_hook():
    hook_data = json.loads(request.data)
    event = hook_data['description']
    # Check if we must forward hook
    if event in allowed_hooks:
        # Forward the hook
        r = requests.post(request.args['url'], json=hook_data)
        if r.status_code != 200:
            logging.error(f'Unable to forward hook : {r}')
        else:
            logging.info('Hook forwarded.')
    # HTTP No Content
    return ('', 204)
