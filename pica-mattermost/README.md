## Mattermost

Pour une documentation générale (administration, fonctionnalités...), voir [le wiki](https://wiki.picasoft.net/doku.php?id=technique:adminserv:etherpad:start).

### Introduction

Ce dossier contient une adaptation minimaliste du [Dockerfile officiel](https://github.com/mattermost/mattermost-docker) de Mattermost.

L'idée de garder une copie du Dockerfile sur ce dépôt est motivée par trois choses :

- Aucune image n'est disponible **officiellement** sur le Docker Hub, même s'il en existe
- En cas de problèmes de sécurité (CVE), on pourra directement agir dessus
- On peut changer les arguments du Dockerfile, comme le type d'instance (`team`) et l'UID de l'utilisateur (à retrouver sur le LDAP).

Aussi, on n'utilise pas le système de sauvegarde `WAL-e`, ce qui nous permet d'utiliser une image `postgres` de base plutôt que de rajouter la couche proposée par l'équipe Mattermost.

Enfin, le Docker Compose est adapté à notre configuration.

Nous ne pouvons pas versionner le fichier de configuration car il est très souvent modifié directement depuis la Console Administrateur : le versionner ici aurait pour effet d'annuler les modifications.

### Configuration

Avant de démarrer l'instance Mattermost, il est important d'ajouter des variables d'environnement de configuration pour la base de donnée dans le fichier `secrets/mattermost-db.secrets`.

D'autres variables de configuration sont à ajouter au fichier `secrets/mattermost-exporter.secrets` pour le bon fonctionnement de [l'exporter Prometheus](prometheus-exporter/README.md).

Enfin il faut créer un fichier `.env` (dans le même dossier que le Docker Compose) qui devra contenir une variable `METRICS_AUTH`. Cette vairbale correspond à la chaîne d'identification htpasswd utilisée pour authentifier sur l'endpoint des métriques, par exemple `METRICS_AUTH="mattermost:$apr1$bXnknJ0S$GsC.ozNJc/dAkh9uH7Qlg."`

### Procédure de mise à jour

Il suffit de changer l'argument correspondant à la version dans le `Dockerfile` ainsi que le tag d'image dans le `docker-compose.yml`. Régulièrement, on vérifiera l'upstream pour s'assurer qu'il n'y a pas de changements majeurs, auxquel cas on les intègrera dans le `Dockerfile` local.

Ce n'est pas le plus pratique, mais ni la CI ni Docker ne permet de reprendre un `Dockerfile` distant et d'y intégrer des modifications.

### Mise à jour du SGBD

Il peut arriver que la version de PostgreSQL ne soit plus supportée par Mattermost.
Sans en arriver là, il est bon de régulièrement mettre à jour PostgreSQL :

> While upgrading will always contain some level of risk, PostgreSQL minor releases fix only frequently-encountered bugs, security issues, and data corruption problems to reduce the risk associated with upgrading. For minor releases, the community considers not upgrading to be riskier than upgrading. https://www.postgresql.org/support/versioning/

Les mise à jours mineures (changement du Y de la version X.Y) peuvent se faire sans intervention humaine. On veillera à bien regarder les logs.

En revanche, le passage d'une version majeure à une autre nécessitera une intervention manuelle.

La documentation complète est ici : https://www.postgresql.org/docs/current/upgrading.html

De manière générale, la façon la plus simple est de se rendre dans l'ancien conteneur, de réaliser un `pg_dumpall` et de le copier en lieu sûr (`docker cp`).
Ensuite, on supprime l'ancien volume de base de données, on relance le nouveau conteneur de base de données (qui sera sans donnée), on monte le fichier de dump, et on lance un `psql -U <user> -d <db> -f <dump_file>` (valeurs de `user` et `db` à matcher avec le fichiers de secrets).

On attend, et **si tout s'est bien passé**, on peut lancer le conteneur applicatif (Mattermost).
