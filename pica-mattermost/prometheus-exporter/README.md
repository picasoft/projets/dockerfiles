# Mattermost Prometheus exporter

Afin d'exporter des métriques de Mattermost pour la métrologie, on utilise cette image Docker qui se charge simplement d'éxécuter un script qui va collecter des informations en base et les exposer sur un endpoint HTTP.

Ce script lit les variables d'environnement suivantes pour sa configuration :

- `EXPORTER_DB_HOST`: hostname du PostgreSQL de Mattermost (par défaut `mattermost-db`)
- `EXPORTER_DB_PORT`: port du PostgreSQL de Mattermost (par défaut `5432`)
- `EXPORTER_DB_NAME`: nom de la base de donnée (par défaut `mattermost`)
- `EXPORTER_DB_USER`: utilisateur pour se connecter à la base (par défaut `mattermost`)
- `EXPORTER_DB_PASSWORD`: mot de passe pour se connecter à la base
- `EXPORTER_COLLECT_INTERVAL`: nombre de secondes entre 2 actualisations des métriques (par défaut `60`)
- `INSTANCE_NAME`: nom de l'instance à ajouter en tag aux métriques (par exemple `team.picasoft.net`)

Pour des raisons de sécurité, il est préférable d'utiliser un compte ayant des droits limités sur la base de données (lecture seule) pour l'exporter. Il est possible de créer ce compte avec les lignes suivantes :

```sql
CREATE USER "mattermost-exporter" WITH PASSWORD 'strongpassword';
GRANT USAGE ON SCHEMA public TO "mattermost-exporter";
GRANT SELECT ON ALL TABLES IN SCHEMA public TO "mattermost-exporter";
```
